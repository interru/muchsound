(window.crypt = function () {
  Crypto = function(secret) {
    this.secret = secret;
  };

  Crypto.prototype.encrypt = function(message) {
    var random = new Int32Array(8);
    var pad = [];

    window.crypto.getRandomValues(random);
    for (var i = 0; i < random.length; i++) {
      pad[i] = random[i];
    }

    message = Bit.utf8_encode(message);
    message = Bit.utf8_to_wordarray(message);
    message = pad.concat(message);

    var length = Math.ceil(message.length / 8) * 8;
    var ctr = (new HMAC(this.secret)).process("" + length).h;
    var encrypted = [];

    while (message.length > 0) {
      next_chunk = message.slice(0, 8);

      encrypted = encrypted.concat(Bit.xor_array(ctr, next_chunk));
      message = message.slice(8);

      ctr = new SHA256(ctr.concat(next_chunk));
      ctr.finalize();
      ctr = ctr.h;
    }

    return Bit.wordarray_to_utf8(encrypted);
  };

  Crypto.prototype.decrypt = function(message) {
    message = Bit.utf8_to_wordarray(message);
    var length = message.length;
    var ctr = (new HMAC(this.secret)).process("" + length).h;
    var decrypted = [];

    while (message.length > 0) {
      next_chunk = message.slice(0, 8);

      next_chunk = Bit.xor_array(ctr, next_chunk);
      decrypted = decrypted.concat(next_chunk);
      message = message.slice(8);

      ctr = new SHA256(ctr.concat(next_chunk));
      ctr.finalize();
      ctr = ctr.h;
    };

    decrypted = Bit.wordarray_to_utf8(decrypted.slice(8))
    decrypted = decrypted.replace(/\0+$/, '')  // Remove padding

    return decrypted;
  };


  SHA256 = function(message) {
    this.reset();
    this.update(message);
  };


  SHA256.prototype.reset = function() {
    var primes = function(start, end, callback) {
      for (var num = start; num < end; num++) {
        var prime = true;
        for (var div = 2; div*div <= num; div++) {
          if (num % div === 0) {
            prime = false;
            div = num;
          }
        }
        if (prime) {
          callback(num);
        }
      }
    };

    var fraction = function(num) {
      return ((num % 1) * 0x100000000) >>> 0;
    };

    this.h = [];
    this.k = [];
    this.buffer = [];
    this.length = 0;
    this.finalized = false;

    var that = this;
    primes(2, 312, function(prime) {
      if (prime < 20) {
        that.h.push(fraction(Math.pow(prime, 1/2)));
      }
      that.k.push(fraction(Math.pow(prime, 1/3)));
    });
  };


  // TODO: Tests if in the right state
  SHA256.prototype.update = function(message, encode) {
    var next_chunk;
    if (typeof encode == 'undefined') {
      encode = true;
    }

    if (this.finalized) {
      throw new Error("Can't update hash because it's already in final state");
    }

    if (typeof message === "string") {
      if (encode) {
        message = Bit.utf8_encode(message);
      }
      this.buffer = this.buffer.concat(Bit.utf8_to_wordarray(message));
    } else {
      this.buffer = this.buffer.concat(message);
    }
    this.length += message.length;

    while (this.buffer.length > 16) {
      next_chunk = this.buffer.slice(0, 16);

      this.process(next_chunk);
      this.buffer = this.buffer.slice(16);
    }
  };


  SHA256.prototype.finalize = function() {
    var word, last_chunk, last_byte, final_chunks, padding;
    this.finalized = true;

    if (this.length % 4 == 0) {
      this.buffer.push(0x80 << 24)
    } else {
      var last_index = this.buffer.length - 1;
      this.buffer[last_index] ^= (0x80 << (8 * (3 - (this.length % 4))))
    }

    word = this.buffer.length - 1;
    last_chunk = ((word+2 >> 4)+1 << 4);
    last_byte = last_chunk << 2;

    final_chunks = this.buffer;
    var padding = [];
    var padding_length = last_chunk - this.buffer.length;
    for (var i = 0; i < padding_length; i++) {
      padding[i] = 0;
    }

    final_chunks = final_chunks.concat(padding);
    final_chunks[last_chunk-2] |= (this.length / 0x100000000) | 0;
    final_chunks[last_chunk-1] |= this.length << 3;

    this.process(final_chunks);
  };


  SHA256.prototype.process = function(wordarray) {
    var w, s0, s1, h, k, S0, S1, ch, maj, d, temp;

    w = [];
    k = this.k.slice();

    for (var i=0; i < wordarray.length; i += 16) {
      for (var n=0; n < 64; n++) {
        if (n < 16) {
          w[n] = wordarray[i + n];
        } else {
          s0 = Bit.xor_each(Bit.rotate_each(w[n-15], [7, 18])) ^ (w[n-15] >>> 3);
          s1 = Bit.xor_each(Bit.rotate_each(w[n-2], [17, 19])) ^ (w[n-2] >>> 10);
          w[n] = (w[n-16] + s0 + w[n-7] + s1) & 0xffffffff;
        }
      }

      h = this.h.slice();
      for (var r=0; r < 64; r++) {
        S0 = Bit.xor_each(Bit.rotate_each(h[0], [2, 13, 22]));
        S1 = Bit.xor_each(Bit.rotate_each(h[4], [6, 11, 25]));

        maj = (h[0] & (h[1] ^ h[2])) ^ (h[1] & h[2]);
        ch = (h[4] & h[5]) ^ ((~h[4]) & h[6]);
        temp = (h[7] + S1 + ch + k[r] + w[r]);

        h[3] = (h[3] + temp) & 0xffffffff;
        temp = (temp + S0 + maj) & 0xffffffff;

        h.pop();
        h.unshift(temp);
      }

      var that = this
      this.h.forEach(function(item, index, array) {
        that.h[index] = (item + h[index]) | 0;
      });
    }
  };


  SHA256.prototype.hexdigest = function() {
    if (!this.finalized) {
      this.finalize();
    }

    return Bit.hexdigest(this.h);
  };


  var HMAC = function(key) {
    if (typeof key !== "string") {
      throw new Error("Only strings are allowed as keys");
    }
    if (Bit.utf8_encode(key).length > 64) {
      hash = new SHA256(key);
      hash.finalize();
      this.key = hash.h;
    } else {
      var left = Bit.utf8_to_wordarray(key);
      var right = new Array(16 - left.length);
      for (var i = 0; i < right.length; i++) {
        right[i] = 0;
      }
      this.key = left.concat(right);
    }
  };


  HMAC.prototype.process = function(message) {
    var length, opad, ipad, hash, left, right;

    // Size of Block times 0x5C and 0x36
    opad = Bit.utf8_to_wordarray(new Array(65).join("\\"));
    ipad = Bit.utf8_to_wordarray(new Array(65).join("6"));

    message = Bit.utf8_to_wordarray(message);

    left = Bit.xor_array(this.key, opad);
    right = Bit.xor_array(this.key, ipad);
    right = right.concat(message);

    // hash(o_key_pad ∥ hash(i_key_pad ∥ message))
    right = new SHA256(right);
    right.finalize();
    right = right.h;

    hash = new SHA256(left.concat(right));
    hash.finalize();

    return hash;
  };


  HMAC.prototype.sign = function(message) {
    var hash = this.process(message).h.slice(0,3);
    return btoa(message) + "|" + btoa(Bit.wordarray_to_utf8(hash));
  };


  HMAC.prototype.unsign = function(message) {
    var hash, hmac;
    message = message.split("|");

    hash = this.process(atob(message[0])).h.slice(0,3);
    hmac = btoa(Bit.wordarray_to_utf8(hash));
    if (message[1] !== hmac) {
      throw new Error("Bad signature!");
    }

    return atob(message[0]);
  };


  Bit = {
    rightrotate: function(word, shift) {
      return (word >>> shift) | (word << (32 - shift));
    },

    xor_each: function(values) {
      return values.reduce(function(value, add) {
        return value ^ add;
      });
    },

    rotate_each: function(word, shifts) {
      var words, that = this;
      return shifts.map(function(shift){
        return that.rightrotate(word, shift);
      });
    },

    xor_array: function(left, right) {
      result = [];
      left.forEach(function(item, index, array) {
        result[index] = item ^ right[index];
      });
      return result;
    },

    utf8_to_wordarray: function(str, length) {
      if (typeof length === 'undefined') {
        length = str.length;
      }

      var wordarray = [];
      for (var i=0; i < length; i++) {
        wordarray[i >> 2] |= str.charCodeAt(i) << (8 * (3 - i%4));
      }
      return wordarray;
    },

    wordarray_to_utf8: function(wordarray) {
      var str = "";
      wordarray.forEach(function(item, index, array) {
        for (var i=0; i < 4; i++) {
          str += String.fromCharCode((item >>> ((3-i) * 8)) & 0xff);
        };
      });
      return str;
    },

    utf8_encode: function(message) {
      return unescape(encodeURIComponent(message));
    },

    utf8_decode: function(message) {
      return decodeURIComponent(escape(message));
    },

    digest: function(message, num, length) {
      var result = "";
      var pad = (new Array(length + 1)).join('0');
      message.forEach(function(item, index, array) {
        item = item >>> 0;
        result += (pad + item.toString(num)).slice(-length);
      });
      return result;
    },

    hexdigest: function(message) {
      return this.digest(message, 16, 8);
    },

    bindigest: function(message) {
      return this.digest(message, 2, 32);
    }
  };

  window.Bit = Bit;
  window.HMAC = HMAC;
  window.SHA256 = SHA256;
  window.Crypto = Crypto;
});
