(window.client = function () {
  Client = function(sock, gid, group) {
    this.sock = sock;
    this.heartbeats = 0;
    this.last_heartbeat = null;
    this.group = group || null;
    this.name = null;

    if (typeof gid === 'undefined' || gid === null) {
      this.gid = [];
      var random = new Uint32Array(4);

      window.crypto.getRandomValues(random);
      for (var i = 0; i < random.length; i++) {
        this.gid[i] = random[i];
      }

      this.gid = Bit.hexdigest(this.gid);
    } else {
      this.gid = gid;
    }
  };

  Client.prototype.join_group = function(name) {
    this.group = name;
    try {
      this.sock.send_heartbeat();
    } catch (e) {}
  };

  Client.prototype.change_name = function(name) {
    this.name = name;
    try {
      this.sock.send_heartbeat();
    } catch (e) {}
    if (typeof this.on_name_change === 'function') {
      this.on_name_change();
    }
  };

  window.Client = Client;
});

