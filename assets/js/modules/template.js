(window.templates = function () {
  Templates = function() {
    var that = this;
    this.loaded = false;
    this.templates = document.createElement('templates');
  };

  Templates.prototype.load = function() {
    this.started = (new Date).getTime();

    var httpRequest;
    if (window.XMLHttpRequest) {
      httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
      httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var that = this;

    var script = document.getElementById('templates');
    httpRequest.open('GET', script.src, true);
    httpRequest.send(null);

    var promise = new Promise(function(resolve, reject) {
      httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState === 4) {
          if (httpRequest.status === 200) {
            that.templates.innerHTML = httpRequest.responseText;
            that.loaded = true;
            resolve();
          } else {
            reject();
          }
        }
      };
    });

    return promise;
  };

  Templates.prototype.render = function(name, data, wrap) {
    var template = this.templates.querySelector('#' + name);

    var temp = document.createElement('div');
    temp.innerHTML = template.innerHTML;
    temp.id = name;

    data = data || {};
    that = this;

    for (var item in data) {
      var tags = [].slice.call(temp.querySelectorAll('.' + item));
      tags.forEach(function(tag) {
        if (data[item] instanceof Array) {
          data[item][1].forEach(function(innerItem) {
            var inner = that.render(data[item][0], innerItem);
            tag.insertAdjacentHTML('beforeend', inner);
          });
          if (data[item][1].length < 1) {
            tag.parentNode.removeChild(tag);
          }
        } else {
          tag.innerHTML = data[item];
        }
      });
    }

    if (typeof wrap !== 'undefined' && wrap) {
      return temp;
    } else {
      return temp.innerHTML;
    }
  };

  window.Templates = Templates;
});
