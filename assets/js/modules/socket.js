(window.socket = function () {
  Socket = function(url, secret) {
    this.sock = new WebSocket(url);
    this.secret = secret;
    this.crypt = new Crypto(secret);
    this.hmac = new HMAC(secret + "sign");

    this.heartbeats = 0;
    this.client = new Client(this);
    this.clients = [];

    var that = this;
    this.sock.onmessage = function(message) {
      that._onmessage(message);
    };

    this.sock.onopen = function() {
      setInterval(function() {
        that.send_heartbeat();
        that.clean_clients();
      }, 3000);
    };

    this.sock.onclose = function() {
      that.clean_clients(true);
    };
  };

  Socket.prototype.raw_send = function(message) {
    message = this.crypt.encrypt(message);
    message = this.hmac.sign(message);

    if (this.sock.readyState !== 1) {
      throw new Error('Socket isn\'t connected');
    }
    if (message.length > 2000) {
      throw new Error('Message to big');
    }

    this.sock.send(message);
  };

  Socket.prototype.clean_clients = function(all)  {
    var now = (new Date()).getTime();
    if (typeof all == 'undefined') {
      all = false;
    }

    var that = this;
    this.clients.forEach(function(client, index, array) {
      if (all || (now - client.last_heartbeat) > 10000) {
        console.log('Client leaved: ' + client.gid);
        delete array[index];
        if (typeof that.on_leave === 'function') {
          that.on_leave(client);
        }
      }
    });
    this.clients = this.clients.filter(function(item) {
      return item !== 'undefined';
    });
  };

  Socket.prototype.get_groups = function() {
    var groups = {};
    this.clients.forEach(function(client, index, array) {
      if (client.group) {
        if (!(client.group in groups)) {
          groups[client.group] = [];
        }
        groups[client.group].push(client);
      }
    });

    return groups;
  };

  Socket.prototype.send_heartbeat = function() {
    message = {
      type: 'beat',
      gid: this.client.gid
    };

    if (this.client.group !== null) {
      message['group'] = this.client.group;
    }
    if (this.client.name !== null) {
      message['name'] = this.client.name;
    }
    this.raw_send(JSON.stringify(message));
    this.client.heartbeats += 1;
  };

  Socket.prototype.send_event = function(name, msg) {
    message = {
      type: 'event',
      gid: this.client.gid,
      name: name,
      msg: JSON.stringify(msg)
    };
    console.log(message);
    this.raw_send(JSON.stringify(message));
  }

  Socket.prototype.process_beat = function(data) {
    var client = this.clients.filter(function(client) {
      return client.gid === data.gid;
    })[0];

    if (typeof client == 'undefined') {
      console.log('New client: ' + data.gid);
      client = new Client(this, data.gid, data.group || null);
      client.name = data.name || null;
      this.clients.push(client);

      if (this.client.heartbeats > 1) {
        console.log('Introduce!');
        this.send_heartbeat();
      }

      client.last_heartbeat = (new Date()).getTime();
      if (typeof this.on_join === 'function') {
        this.on_join(client);
      }
    } else {
      client.last_heartbeat = (new Date()).getTime();

      var group = data.group || null;
      if (client.group !== group) {
        console.log('Client' + client.gid + ' joined group: ' + group);
        client.group = group;
        if (typeof this.on_group_change === 'function') {
          this.on_group_change(client);
        }
      }
      var name = data.name || null;
      if (client.name !== name) {
        console.log('Client' + client.gid + ' changed name: ' + name);
        client.name = name;
        if (typeof this.on_name_change === 'function') {
          this.on_name_change(client);
        }
      }
    }
  };

  Socket.prototype._onmessage = function(message) {
    var data;
    try {
      data = this.hmac.unsign(message.data);
      data = this.crypt.decrypt(data);
      // Parse as JSON (we only send json)
      data = JSON.parse(data);
    } catch (e) {
      // HMAC failed. We didn't send the message.
      data = message.data;
      console.log(data);
    }

    if (data.type === 'beat') {
      this.process_beat(data);
    } else if (data.type === 'event') {
      console.log(data);
      var client = this.clients.filter(function(client) {
        return client.gid === data.gid;
      })[0];
      console.log(client);
      if (typeof this['on_' + data.name] === 'function') {
        console.log('Soweit sogut: lol');
        console.log(client);
        this['on_' + data.name](client, JSON.parse(data.msg));
      }
    }
  };

  window.Socket = Socket;
});
