(window.main = function () {
  var sock = new Socket(
    'ws://127.0.0.1:8000/server',
    'Die ist ein Sekret'
  );
  var t = new Templates();

  var init_canvas = function() {
    var canvas = document.getElementById("play_area");
    canvas.width = Math.floor(canvas.parentNode.clientWidth);
    canvas.height = Math.floor(window.innerHeight);
    var ctx = canvas.getContext("2d");

    var line_every = canvas.height / 30;
    for (var i = line_every; i < canvas.height; i += line_every) {
      ctx.lineWidth = 0.5;
      ctx.strokeStyle = "rgba(41,36,32,0.6)";
      ctx.beginPath();
      var ceil = Math.ceil(i)+0.5
      ctx.moveTo(0, ceil);
      ctx.lineTo(canvas.width, ceil);
      ctx.stroke();
    }
  };

  var header = document.getElementsByTagName('header')[0];
  header.onclick = function() {
    location.hash = 'index';
  };

  Route = function(start) {
    location.hash = start;
    this[location.hash.slice(1)]();
    this.current = location.hash.slice(1);
    var that = this;

    window.onhashchange = function() {
      if (typeof that.current !== 'undefined') {
        try {
          that[that.current + "_exit"]();
        } catch (e) {
          if (!(e instanceof ReferenceError)) {
            throw e;
          }
        }
      }
      that[location.hash.slice(1)]();
      that.current = location.hash.slice(1);
    };
  };


  Route.prototype.index = function() {
    sock.client.join_group(null);

    // Display Welcome
    var welcome = t.render('welcome');
    var content = document.getElementById('content');
    content.innerHTML = welcome;

    // Username changer
    var sidebar = document.getElementById('controls');
    var name_form = t.render('name_form');
    sidebar.insertAdjacentHTML('beforeend', name_form);
    var username = document.getElementById('username');
    username.value = sock.client.name || '';

    name_form = username.parentNode;
    name_form.onsubmit = function(e) {
      console.log(username.value);
      e.preventDefault();
      sock.client.change_name(username.value);
    };

    // Append Groupname Handler
    var group_name = document.getElementById('group_name');
    var group_form = document.getElementById('group_form');
    group_form.onsubmit = function(e) {
      e.preventDefault();
      sock.client.join_group(group_name.value);
      location.hash = 'group';
    };
    group_name.focus();

    // Create Group Wrapper
    var groups_tag = document.createElement('ul');
    groups_tag.className = "groups";
    sidebar.appendChild(groups_tag);

    // Render function group display
    var render_groups = function() {
      var groups = sock.get_groups();

      groups_tag.innerHTML = "";
      for (var group in groups) {
        var users = groups[group].filter(function(client) {
          return client.name !== null
        });
        var anonymous = groups[group].length - users.length;

        users = users.map(function(client) {
          return {user_name: client.name}
        });

        var entry = t.render('group_entry', {
            name: group,
            users: ['user_entry', users]
        }, true);
        entry = entry.firstElementChild;

        entry.dataset.name = group;
        entry.onclick = function() {
          sock.client.join_group(entry.dataset.name);
          location.hash = 'group';
        };
        groups_tag.appendChild(entry);
      }
    };

    // Invoke render function on join and group change
    sock.on_group_change = render_groups;
    sock.on_join = render_groups;
    sock.on_name_change = render_groups;
    sock.on_leave = render_groups;

    init_canvas();
    window.onresize = init_canvas;
  };

  Route.prototype.index_exit = function() {
    delete sock.on_group_change;
    delete sock.on_join;
    delete sock.on_name_change;
    delete sock.on_leave;

    var sidebar = document.getElementById('controls');
    sidebar.innerHTML = '';

    var content = document.getElementById('content');
    content.innerHTML = '';
  };

  Route.prototype.group = function() {
    if (!sock.client.group) {
      location.hash = 'index';
    }

    var sidebar = document.getElementById('controls');
    var render_chat = function() {
      var chat = t.render('chat');
      sidebar.insertAdjacentHTML('beforeend', chat);
      chat = document.getElementById('chat');

      sock.on_message = function(client, data) {
        var messages = document.getElementById('messages');
        console.log(client.name);
        console.log(data.msg);
        var message = t.render('message', {
          'user': client.name,
          'msg_content': data
        });
        messages.insertAdjacentHTML('beforeend', message);
        messages.scrollTop = messages.scrollHeight;
      };

      var form = chat.getElementsByTagName('form')[0];
      var input = document.getElementById('message_input');
      form.onsubmit = function(e) {
        e.preventDefault();

        if (input.value) {
          sock.send_event('message', input.value);
          sock.on_message(sock.client, input.value);
        }
        input.value = "";
      };
      input.focus();
    }

    if (sock.client.name !== null) {
      render_chat();
    } else {
      var name_form = t.render('name_form');
      sidebar.insertAdjacentHTML('beforeend', name_form);
      var username = document.getElementById('username');
      name_form = username.parentNode;
      name_form.onsubmit = function(e) {
        console.log(username.value);
        e.preventDefault();
        sock.client.change_name(username.value);
        name_form.parentNode.removeChild(name_form);
        render_chat();
      };
      username.focus();
    }
  };

  Route.prototype.group_exit = function() {
    delete sock.on_message;

    var sidebar = document.getElementById('controls');
    sidebar.innerHTML = '';

    var content = document.getElementById('content');
    content.innerHTML = '';
  };

  t.load().then(function() {
    var route = new Route(location.hash.slice(1) || 'index');
  }, function() {
    throw new Error('Shit');
  });
});
