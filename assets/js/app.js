(function () {
  window.client();
  window.crypt();
  window.socket();
  window.templates();
  window.main();
}());
