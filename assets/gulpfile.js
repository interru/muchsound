var gulp = require('gulp'),
    gutil = require('gulp-util'),
    livereload = require('gulp-livereload'),
    watch = require('gulp-watch'),
    compass = require('gulp-compass'),
    minifyCSS = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps');


gulp.task('styles', function() {
  gulp.src('./sass/*.sass')
      .pipe(compass({
        config_file: './config.rb',
        css: './css',
        sass: './sass',
        project: __dirname
      }))
      .pipe(minifyCSS())
      .pipe(gulp.dest('../static/css'))
      .pipe(livereload());
});


gulp.task('javascript', function() {
  gulp.src(['./js/*/**.js', './js/app.js'])
      .pipe(sourcemaps.init())
      //  .pipe(uglify())
        .pipe(concat('dist.js'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('../static/js'));
});


gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(['./js/*/**.js', './js/app.js'], ['javascript']);
  gulp.watch('./sass/*.sass', ['styles']);
});

gulp.task('default', ['watch']);
