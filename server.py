# -*- coding: utf-8 -*-

from flask import Flask, render_template
from flask_sockets import Sockets


app = Flask(__name__, template_folder='./')
sockets = Sockets(app)
app.connections = []
app.debug = True


@app.route("/")
def index():
    return render_template('index.html')


@sockets.route('/server')
def echo_socket(ws):
    def send_all(sender, msg):
        for ws in app.connections:
            if ws is not sender:
                ws.send(msg)
    try:
        app.connections.append(ws)
        while True:
            message = ws.receive()
            print message
            send_all(ws, message)
    finally:
        app.connections.remove(ws)
