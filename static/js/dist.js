(window.client = function () {
  Client = function(sock, gid, group) {
    this.sock = sock;
    this.heartbeats = 0;
    this.last_heartbeat = null;
    this.group = group || null;
    this.name = null;

    if (typeof gid === 'undefined' || gid === null) {
      this.gid = [];
      var random = new Uint32Array(4);

      window.crypto.getRandomValues(random);
      for (var i = 0; i < random.length; i++) {
        this.gid[i] = random[i];
      }

      this.gid = Bit.hexdigest(this.gid);
    } else {
      this.gid = gid;
    }
  };

  Client.prototype.join_group = function(name) {
    this.group = name;
    try {
      this.sock.send_heartbeat();
    } catch (e) {}
  };

  Client.prototype.change_name = function(name) {
    this.name = name;
    try {
      this.sock.send_heartbeat();
    } catch (e) {}
    if (typeof this.on_name_change === 'function') {
      this.on_name_change();
    }
  };

  window.Client = Client;
});


(window.crypt = function () {
  Crypto = function(secret) {
    this.secret = secret;
  };

  Crypto.prototype.encrypt = function(message) {
    var random = new Int32Array(8);
    var pad = [];

    window.crypto.getRandomValues(random);
    for (var i = 0; i < random.length; i++) {
      pad[i] = random[i];
    }

    message = Bit.utf8_encode(message);
    message = Bit.utf8_to_wordarray(message);
    message = pad.concat(message);

    var length = Math.ceil(message.length / 8) * 8;
    var ctr = (new HMAC(this.secret)).process("" + length).h;
    var encrypted = [];

    while (message.length > 0) {
      next_chunk = message.slice(0, 8);

      encrypted = encrypted.concat(Bit.xor_array(ctr, next_chunk));
      message = message.slice(8);

      ctr = new SHA256(ctr.concat(next_chunk));
      ctr.finalize();
      ctr = ctr.h;
    }

    return Bit.wordarray_to_utf8(encrypted);
  };

  Crypto.prototype.decrypt = function(message) {
    message = Bit.utf8_to_wordarray(message);
    var length = message.length;
    var ctr = (new HMAC(this.secret)).process("" + length).h;
    var decrypted = [];

    while (message.length > 0) {
      next_chunk = message.slice(0, 8);

      next_chunk = Bit.xor_array(ctr, next_chunk);
      decrypted = decrypted.concat(next_chunk);
      message = message.slice(8);

      ctr = new SHA256(ctr.concat(next_chunk));
      ctr.finalize();
      ctr = ctr.h;
    };

    decrypted = Bit.wordarray_to_utf8(decrypted.slice(8))
    decrypted = decrypted.replace(/\0+$/, '')  // Remove padding

    return decrypted;
  };


  SHA256 = function(message) {
    this.reset();
    this.update(message);
  };


  SHA256.prototype.reset = function() {
    var primes = function(start, end, callback) {
      for (var num = start; num < end; num++) {
        var prime = true;
        for (var div = 2; div*div <= num; div++) {
          if (num % div === 0) {
            prime = false;
            div = num;
          }
        }
        if (prime) {
          callback(num);
        }
      }
    };

    var fraction = function(num) {
      return ((num % 1) * 0x100000000) >>> 0;
    };

    this.h = [];
    this.k = [];
    this.buffer = [];
    this.length = 0;
    this.finalized = false;

    var that = this;
    primes(2, 312, function(prime) {
      if (prime < 20) {
        that.h.push(fraction(Math.pow(prime, 1/2)));
      }
      that.k.push(fraction(Math.pow(prime, 1/3)));
    });
  };


  // TODO: Tests if in the right state
  SHA256.prototype.update = function(message, encode) {
    var next_chunk;
    if (typeof encode == 'undefined') {
      encode = true;
    }

    if (this.finalized) {
      throw new Error("Can't update hash because it's already in final state");
    }

    if (typeof message === "string") {
      if (encode) {
        message = Bit.utf8_encode(message);
      }
      this.buffer = this.buffer.concat(Bit.utf8_to_wordarray(message));
    } else {
      this.buffer = this.buffer.concat(message);
    }
    this.length += message.length;

    while (this.buffer.length > 16) {
      next_chunk = this.buffer.slice(0, 16);

      this.process(next_chunk);
      this.buffer = this.buffer.slice(16);
    }
  };


  SHA256.prototype.finalize = function() {
    var word, last_chunk, last_byte, final_chunks, padding;
    this.finalized = true;

    if (this.length % 4 == 0) {
      this.buffer.push(0x80 << 24)
    } else {
      var last_index = this.buffer.length - 1;
      this.buffer[last_index] ^= (0x80 << (8 * (3 - (this.length % 4))))
    }

    word = this.buffer.length - 1;
    last_chunk = ((word+2 >> 4)+1 << 4);
    last_byte = last_chunk << 2;

    final_chunks = this.buffer;
    var padding = [];
    var padding_length = last_chunk - this.buffer.length;
    for (var i = 0; i < padding_length; i++) {
      padding[i] = 0;
    }

    final_chunks = final_chunks.concat(padding);
    final_chunks[last_chunk-2] |= (this.length / 0x100000000) | 0;
    final_chunks[last_chunk-1] |= this.length << 3;

    this.process(final_chunks);
  };


  SHA256.prototype.process = function(wordarray) {
    var w, s0, s1, h, k, S0, S1, ch, maj, d, temp;

    w = [];
    k = this.k.slice();

    for (var i=0; i < wordarray.length; i += 16) {
      for (var n=0; n < 64; n++) {
        if (n < 16) {
          w[n] = wordarray[i + n];
        } else {
          s0 = Bit.xor_each(Bit.rotate_each(w[n-15], [7, 18])) ^ (w[n-15] >>> 3);
          s1 = Bit.xor_each(Bit.rotate_each(w[n-2], [17, 19])) ^ (w[n-2] >>> 10);
          w[n] = (w[n-16] + s0 + w[n-7] + s1) & 0xffffffff;
        }
      }

      h = this.h.slice();
      for (var r=0; r < 64; r++) {
        S0 = Bit.xor_each(Bit.rotate_each(h[0], [2, 13, 22]));
        S1 = Bit.xor_each(Bit.rotate_each(h[4], [6, 11, 25]));

        maj = (h[0] & (h[1] ^ h[2])) ^ (h[1] & h[2]);
        ch = (h[4] & h[5]) ^ ((~h[4]) & h[6]);
        temp = (h[7] + S1 + ch + k[r] + w[r]);

        h[3] = (h[3] + temp) & 0xffffffff;
        temp = (temp + S0 + maj) & 0xffffffff;

        h.pop();
        h.unshift(temp);
      }

      var that = this
      this.h.forEach(function(item, index, array) {
        that.h[index] = (item + h[index]) | 0;
      });
    }
  };


  SHA256.prototype.hexdigest = function() {
    if (!this.finalized) {
      this.finalize();
    }

    return Bit.hexdigest(this.h);
  };


  var HMAC = function(key) {
    if (typeof key !== "string") {
      throw new Error("Only strings are allowed as keys");
    }
    if (Bit.utf8_encode(key).length > 64) {
      hash = new SHA256(key);
      hash.finalize();
      this.key = hash.h;
    } else {
      var left = Bit.utf8_to_wordarray(key);
      var right = new Array(16 - left.length);
      for (var i = 0; i < right.length; i++) {
        right[i] = 0;
      }
      this.key = left.concat(right);
    }
  };


  HMAC.prototype.process = function(message) {
    var length, opad, ipad, hash, left, right;

    // Size of Block times 0x5C and 0x36
    opad = Bit.utf8_to_wordarray(new Array(65).join("\\"));
    ipad = Bit.utf8_to_wordarray(new Array(65).join("6"));

    message = Bit.utf8_to_wordarray(message);

    left = Bit.xor_array(this.key, opad);
    right = Bit.xor_array(this.key, ipad);
    right = right.concat(message);

    // hash(o_key_pad ∥ hash(i_key_pad ∥ message))
    right = new SHA256(right);
    right.finalize();
    right = right.h;

    hash = new SHA256(left.concat(right));
    hash.finalize();

    return hash;
  };


  HMAC.prototype.sign = function(message) {
    var hash = this.process(message).h.slice(0,3);
    return btoa(message) + "|" + btoa(Bit.wordarray_to_utf8(hash));
  };


  HMAC.prototype.unsign = function(message) {
    var hash, hmac;
    message = message.split("|");

    hash = this.process(atob(message[0])).h.slice(0,3);
    hmac = btoa(Bit.wordarray_to_utf8(hash));
    if (message[1] !== hmac) {
      throw new Error("Bad signature!");
    }

    return atob(message[0]);
  };


  Bit = {
    rightrotate: function(word, shift) {
      return (word >>> shift) | (word << (32 - shift));
    },

    xor_each: function(values) {
      return values.reduce(function(value, add) {
        return value ^ add;
      });
    },

    rotate_each: function(word, shifts) {
      var words, that = this;
      return shifts.map(function(shift){
        return that.rightrotate(word, shift);
      });
    },

    xor_array: function(left, right) {
      result = [];
      left.forEach(function(item, index, array) {
        result[index] = item ^ right[index];
      });
      return result;
    },

    utf8_to_wordarray: function(str, length) {
      if (typeof length === 'undefined') {
        length = str.length;
      }

      var wordarray = [];
      for (var i=0; i < length; i++) {
        wordarray[i >> 2] |= str.charCodeAt(i) << (8 * (3 - i%4));
      }
      return wordarray;
    },

    wordarray_to_utf8: function(wordarray) {
      var str = "";
      wordarray.forEach(function(item, index, array) {
        for (var i=0; i < 4; i++) {
          str += String.fromCharCode((item >>> ((3-i) * 8)) & 0xff);
        };
      });
      return str;
    },

    utf8_encode: function(message) {
      return unescape(encodeURIComponent(message));
    },

    utf8_decode: function(message) {
      return decodeURIComponent(escape(message));
    },

    digest: function(message, num, length) {
      var result = "";
      var pad = (new Array(length + 1)).join('0');
      message.forEach(function(item, index, array) {
        item = item >>> 0;
        result += (pad + item.toString(num)).slice(-length);
      });
      return result;
    },

    hexdigest: function(message) {
      return this.digest(message, 16, 8);
    },

    bindigest: function(message) {
      return this.digest(message, 2, 32);
    }
  };

  window.Bit = Bit;
  window.HMAC = HMAC;
  window.SHA256 = SHA256;
  window.Crypto = Crypto;
});

(window.main = function () {
  var sock = new Socket(
    'ws://127.0.0.1:8000/server',
    'Die ist ein Sekret'
  );
  var t = new Templates();

  var init_canvas = function() {
    var canvas = document.getElementById("play_area");
    canvas.width = Math.floor(canvas.parentNode.clientWidth);
    canvas.height = Math.floor(window.innerHeight);
    var ctx = canvas.getContext("2d");

    var line_every = canvas.height / 30;
    for (var i = line_every; i < canvas.height; i += line_every) {
      ctx.lineWidth = 0.5;
      ctx.strokeStyle = "rgba(41,36,32,0.6)";
      ctx.beginPath();
      var ceil = Math.ceil(i)+0.5
      ctx.moveTo(0, ceil);
      ctx.lineTo(canvas.width, ceil);
      ctx.stroke();
    }
  };

  var header = document.getElementsByTagName('header')[0];
  header.onclick = function() {
    location.hash = 'index';
  };

  Route = function(start) {
    location.hash = start;
    this[location.hash.slice(1)]();
    this.current = location.hash.slice(1);
    var that = this;

    window.onhashchange = function() {
      if (typeof that.current !== 'undefined') {
        try {
          that[that.current + "_exit"]();
        } catch (e) {
          if (!(e instanceof ReferenceError)) {
            throw e;
          }
        }
      }
      that[location.hash.slice(1)]();
      that.current = location.hash.slice(1);
    };
  };


  Route.prototype.index = function() {
    sock.client.join_group(null);

    // Display Welcome
    var welcome = t.render('welcome');
    var content = document.getElementById('content');
    content.innerHTML = welcome;

    // Username changer
    var sidebar = document.getElementById('controls');
    var name_form = t.render('name_form');
    sidebar.insertAdjacentHTML('beforeend', name_form);
    var username = document.getElementById('username');
    username.value = sock.client.name || '';

    name_form = username.parentNode;
    name_form.onsubmit = function(e) {
      console.log(username.value);
      e.preventDefault();
      sock.client.change_name(username.value);
    };

    // Append Groupname Handler
    var group_name = document.getElementById('group_name');
    var group_form = document.getElementById('group_form');
    group_form.onsubmit = function(e) {
      e.preventDefault();
      sock.client.join_group(group_name.value);
      location.hash = 'group';
    };
    group_name.focus();

    // Create Group Wrapper
    var groups_tag = document.createElement('ul');
    groups_tag.className = "groups";
    sidebar.appendChild(groups_tag);

    // Render function group display
    var render_groups = function() {
      var groups = sock.get_groups();

      groups_tag.innerHTML = "";
      for (var group in groups) {
        var users = groups[group].filter(function(client) {
          return client.name !== null
        });
        var anonymous = groups[group].length - users.length;

        users = users.map(function(client) {
          return {user_name: client.name}
        });

        var entry = t.render('group_entry', {
            name: group,
            users: ['user_entry', users]
        }, true);
        entry = entry.firstElementChild;

        entry.dataset.name = group;
        entry.onclick = function() {
          sock.client.join_group(entry.dataset.name);
          location.hash = 'group';
        };
        groups_tag.appendChild(entry);
      }
    };

    // Invoke render function on join and group change
    sock.on_group_change = render_groups;
    sock.on_join = render_groups;
    sock.on_name_change = render_groups;
    sock.on_leave = render_groups;

    init_canvas();
    window.onresize = init_canvas;
  };

  Route.prototype.index_exit = function() {
    delete sock.on_group_change;
    delete sock.on_join;
    delete sock.on_name_change;
    delete sock.on_leave;

    var sidebar = document.getElementById('controls');
    sidebar.innerHTML = '';

    var content = document.getElementById('content');
    content.innerHTML = '';
  };

  Route.prototype.group = function() {
    if (!sock.client.group) {
      location.hash = 'index';
    }

    var sidebar = document.getElementById('controls');
    var render_chat = function() {
      var chat = t.render('chat');
      sidebar.insertAdjacentHTML('beforeend', chat);
      chat = document.getElementById('chat');

      sock.on_message = function(client, data) {
        var messages = document.getElementById('messages');
        console.log(client.name);
        console.log(data.msg);
        var message = t.render('message', {
          'user': client.name,
          'msg_content': data
        });
        messages.insertAdjacentHTML('beforeend', message);
        messages.scrollTop = messages.scrollHeight;
      };

      var form = chat.getElementsByTagName('form')[0];
      var input = document.getElementById('message_input');
      form.onsubmit = function(e) {
        e.preventDefault();

        if (input.value) {
          sock.send_event('message', input.value);
          sock.on_message(sock.client, input.value);
        }
        input.value = "";
      };
      input.focus();
    }

    if (sock.client.name !== null) {
      render_chat();
    } else {
      var name_form = t.render('name_form');
      sidebar.insertAdjacentHTML('beforeend', name_form);
      var username = document.getElementById('username');
      name_form = username.parentNode;
      name_form.onsubmit = function(e) {
        console.log(username.value);
        e.preventDefault();
        sock.client.change_name(username.value);
        name_form.parentNode.removeChild(name_form);
        render_chat();
      };
      username.focus();
    }
  };

  Route.prototype.group_exit = function() {
    delete sock.on_message;

    var sidebar = document.getElementById('controls');
    sidebar.innerHTML = '';

    var content = document.getElementById('content');
    content.innerHTML = '';
  };

  t.load().then(function() {
    var route = new Route(location.hash.slice(1) || 'index');
  }, function() {
    throw new Error('Shit');
  });
});

(window.socket = function () {
  Socket = function(url, secret) {
    this.sock = new WebSocket(url);
    this.secret = secret;
    this.crypt = new Crypto(secret);
    this.hmac = new HMAC(secret + "sign");

    this.heartbeats = 0;
    this.client = new Client(this);
    this.clients = [];

    var that = this;
    this.sock.onmessage = function(message) {
      that._onmessage(message);
    };

    this.sock.onopen = function() {
      setInterval(function() {
        that.send_heartbeat();
        that.clean_clients();
      }, 3000);
    };

    this.sock.onclose = function() {
      that.clean_clients(true);
    };
  };

  Socket.prototype.raw_send = function(message) {
    message = this.crypt.encrypt(message);
    message = this.hmac.sign(message);

    if (this.sock.readyState !== 1) {
      throw new Error('Socket isn\'t connected');
    }
    if (message.length > 2000) {
      throw new Error('Message to big');
    }

    this.sock.send(message);
  };

  Socket.prototype.clean_clients = function(all)  {
    var now = (new Date()).getTime();
    if (typeof all == 'undefined') {
      all = false;
    }

    var that = this;
    this.clients.forEach(function(client, index, array) {
      if (all || (now - client.last_heartbeat) > 10000) {
        console.log('Client leaved: ' + client.gid);
        delete array[index];
        if (typeof that.on_leave === 'function') {
          that.on_leave(client);
        }
      }
    });
    this.clients = this.clients.filter(function(item) {
      return item !== 'undefined';
    });
  };

  Socket.prototype.get_groups = function() {
    var groups = {};
    this.clients.forEach(function(client, index, array) {
      if (client.group) {
        if (!(client.group in groups)) {
          groups[client.group] = [];
        }
        groups[client.group].push(client);
      }
    });

    return groups;
  };

  Socket.prototype.send_heartbeat = function() {
    message = {
      type: 'beat',
      gid: this.client.gid
    };

    if (this.client.group !== null) {
      message['group'] = this.client.group;
    }
    if (this.client.name !== null) {
      message['name'] = this.client.name;
    }
    this.raw_send(JSON.stringify(message));
    this.client.heartbeats += 1;
  };

  Socket.prototype.send_event = function(name, msg) {
    message = {
      type: 'event',
      gid: this.client.gid,
      name: name,
      msg: JSON.stringify(msg)
    };
    console.log(message);
    this.raw_send(JSON.stringify(message));
  }

  Socket.prototype.process_beat = function(data) {
    var client = this.clients.filter(function(client) {
      return client.gid === data.gid;
    })[0];

    if (typeof client == 'undefined') {
      console.log('New client: ' + data.gid);
      client = new Client(this, data.gid, data.group || null);
      client.name = data.name || null;
      this.clients.push(client);

      if (this.client.heartbeats > 1) {
        console.log('Introduce!');
        this.send_heartbeat();
      }

      client.last_heartbeat = (new Date()).getTime();
      if (typeof this.on_join === 'function') {
        this.on_join(client);
      }
    } else {
      client.last_heartbeat = (new Date()).getTime();

      var group = data.group || null;
      if (client.group !== group) {
        console.log('Client' + client.gid + ' joined group: ' + group);
        client.group = group;
        if (typeof this.on_group_change === 'function') {
          this.on_group_change(client);
        }
      }
      var name = data.name || null;
      if (client.name !== name) {
        console.log('Client' + client.gid + ' changed name: ' + name);
        client.name = name;
        if (typeof this.on_name_change === 'function') {
          this.on_name_change(client);
        }
      }
    }
  };

  Socket.prototype._onmessage = function(message) {
    var data;
    try {
      data = this.hmac.unsign(message.data);
      data = this.crypt.decrypt(data);
      // Parse as JSON (we only send json)
      data = JSON.parse(data);
    } catch (e) {
      // HMAC failed. We didn't send the message.
      data = message.data;
      console.log(data);
    }

    if (data.type === 'beat') {
      this.process_beat(data);
    } else if (data.type === 'event') {
      console.log(data);
      var client = this.clients.filter(function(client) {
        return client.gid === data.gid;
      })[0];
      console.log(client);
      if (typeof this['on_' + data.name] === 'function') {
        console.log('Soweit sogut: lol');
        console.log(client);
        this['on_' + data.name](client, JSON.parse(data.msg));
      }
    }
  };

  window.Socket = Socket;
});

(window.templates = function () {
  Templates = function() {
    var that = this;
    this.loaded = false;
    this.templates = document.createElement('templates');
  };

  Templates.prototype.load = function() {
    this.started = (new Date).getTime();

    var httpRequest;
    if (window.XMLHttpRequest) {
      httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
      httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var that = this;

    var script = document.getElementById('templates');
    httpRequest.open('GET', script.src, true);
    httpRequest.send(null);

    var promise = new Promise(function(resolve, reject) {
      httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState === 4) {
          if (httpRequest.status === 200) {
            that.templates.innerHTML = httpRequest.responseText;
            that.loaded = true;
            resolve();
          } else {
            reject();
          }
        }
      };
    });

    return promise;
  };

  Templates.prototype.render = function(name, data, wrap) {
    var template = this.templates.querySelector('#' + name);

    var temp = document.createElement('div');
    temp.innerHTML = template.innerHTML;
    temp.id = name;

    data = data || {};
    that = this;

    for (var item in data) {
      var tags = [].slice.call(temp.querySelectorAll('.' + item));
      tags.forEach(function(tag) {
        if (data[item] instanceof Array) {
          data[item][1].forEach(function(innerItem) {
            var inner = that.render(data[item][0], innerItem);
            tag.insertAdjacentHTML('beforeend', inner);
          });
          if (data[item][1].length < 1) {
            tag.parentNode.removeChild(tag);
          }
        } else {
          tag.innerHTML = data[item];
        }
      });
    }

    if (typeof wrap !== 'undefined' && wrap) {
      return temp;
    } else {
      return temp.innerHTML;
    }
  };

  window.Templates = Templates;
});

(function () {
  window.client();
  window.crypt();
  window.socket();
  window.templates();
  window.main();
}());

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzdC5qcyIsInNvdXJjZXMiOlsibW9kdWxlcy9jbGllbnQuanMiLCJtb2R1bGVzL2NyeXB0by5qcyIsIm1vZHVsZXMvbWFpbi5qcyIsIm1vZHVsZXMvc29ja2V0LmpzIiwibW9kdWxlcy90ZW1wbGF0ZS5qcyIsImFwcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDM0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcldBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDck5BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDakxBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM1RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbIih3aW5kb3cuY2xpZW50ID0gZnVuY3Rpb24gKCkge1xuICBDbGllbnQgPSBmdW5jdGlvbihzb2NrLCBnaWQsIGdyb3VwKSB7XG4gICAgdGhpcy5zb2NrID0gc29jaztcbiAgICB0aGlzLmhlYXJ0YmVhdHMgPSAwO1xuICAgIHRoaXMubGFzdF9oZWFydGJlYXQgPSBudWxsO1xuICAgIHRoaXMuZ3JvdXAgPSBncm91cCB8fCBudWxsO1xuICAgIHRoaXMubmFtZSA9IG51bGw7XG5cbiAgICBpZiAodHlwZW9mIGdpZCA9PT0gJ3VuZGVmaW5lZCcgfHwgZ2lkID09PSBudWxsKSB7XG4gICAgICB0aGlzLmdpZCA9IFtdO1xuICAgICAgdmFyIHJhbmRvbSA9IG5ldyBVaW50MzJBcnJheSg0KTtcblxuICAgICAgd2luZG93LmNyeXB0by5nZXRSYW5kb21WYWx1ZXMocmFuZG9tKTtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcmFuZG9tLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHRoaXMuZ2lkW2ldID0gcmFuZG9tW2ldO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmdpZCA9IEJpdC5oZXhkaWdlc3QodGhpcy5naWQpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmdpZCA9IGdpZDtcbiAgICB9XG4gIH07XG5cbiAgQ2xpZW50LnByb3RvdHlwZS5qb2luX2dyb3VwID0gZnVuY3Rpb24obmFtZSkge1xuICAgIHRoaXMuZ3JvdXAgPSBuYW1lO1xuICAgIHRyeSB7XG4gICAgICB0aGlzLnNvY2suc2VuZF9oZWFydGJlYXQoKTtcbiAgICB9IGNhdGNoIChlKSB7fVxuICB9O1xuXG4gIENsaWVudC5wcm90b3R5cGUuY2hhbmdlX25hbWUgPSBmdW5jdGlvbihuYW1lKSB7XG4gICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICB0cnkge1xuICAgICAgdGhpcy5zb2NrLnNlbmRfaGVhcnRiZWF0KCk7XG4gICAgfSBjYXRjaCAoZSkge31cbiAgICBpZiAodHlwZW9mIHRoaXMub25fbmFtZV9jaGFuZ2UgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHRoaXMub25fbmFtZV9jaGFuZ2UoKTtcbiAgICB9XG4gIH07XG5cbiAgd2luZG93LkNsaWVudCA9IENsaWVudDtcbn0pO1xuXG4iLCIod2luZG93LmNyeXB0ID0gZnVuY3Rpb24gKCkge1xuICBDcnlwdG8gPSBmdW5jdGlvbihzZWNyZXQpIHtcbiAgICB0aGlzLnNlY3JldCA9IHNlY3JldDtcbiAgfTtcblxuICBDcnlwdG8ucHJvdG90eXBlLmVuY3J5cHQgPSBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgdmFyIHJhbmRvbSA9IG5ldyBJbnQzMkFycmF5KDgpO1xuICAgIHZhciBwYWQgPSBbXTtcblxuICAgIHdpbmRvdy5jcnlwdG8uZ2V0UmFuZG9tVmFsdWVzKHJhbmRvbSk7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCByYW5kb20ubGVuZ3RoOyBpKyspIHtcbiAgICAgIHBhZFtpXSA9IHJhbmRvbVtpXTtcbiAgICB9XG5cbiAgICBtZXNzYWdlID0gQml0LnV0ZjhfZW5jb2RlKG1lc3NhZ2UpO1xuICAgIG1lc3NhZ2UgPSBCaXQudXRmOF90b193b3JkYXJyYXkobWVzc2FnZSk7XG4gICAgbWVzc2FnZSA9IHBhZC5jb25jYXQobWVzc2FnZSk7XG5cbiAgICB2YXIgbGVuZ3RoID0gTWF0aC5jZWlsKG1lc3NhZ2UubGVuZ3RoIC8gOCkgKiA4O1xuICAgIHZhciBjdHIgPSAobmV3IEhNQUModGhpcy5zZWNyZXQpKS5wcm9jZXNzKFwiXCIgKyBsZW5ndGgpLmg7XG4gICAgdmFyIGVuY3J5cHRlZCA9IFtdO1xuXG4gICAgd2hpbGUgKG1lc3NhZ2UubGVuZ3RoID4gMCkge1xuICAgICAgbmV4dF9jaHVuayA9IG1lc3NhZ2Uuc2xpY2UoMCwgOCk7XG5cbiAgICAgIGVuY3J5cHRlZCA9IGVuY3J5cHRlZC5jb25jYXQoQml0Lnhvcl9hcnJheShjdHIsIG5leHRfY2h1bmspKTtcbiAgICAgIG1lc3NhZ2UgPSBtZXNzYWdlLnNsaWNlKDgpO1xuXG4gICAgICBjdHIgPSBuZXcgU0hBMjU2KGN0ci5jb25jYXQobmV4dF9jaHVuaykpO1xuICAgICAgY3RyLmZpbmFsaXplKCk7XG4gICAgICBjdHIgPSBjdHIuaDtcbiAgICB9XG5cbiAgICByZXR1cm4gQml0LndvcmRhcnJheV90b191dGY4KGVuY3J5cHRlZCk7XG4gIH07XG5cbiAgQ3J5cHRvLnByb3RvdHlwZS5kZWNyeXB0ID0gZnVuY3Rpb24obWVzc2FnZSkge1xuICAgIG1lc3NhZ2UgPSBCaXQudXRmOF90b193b3JkYXJyYXkobWVzc2FnZSk7XG4gICAgdmFyIGxlbmd0aCA9IG1lc3NhZ2UubGVuZ3RoO1xuICAgIHZhciBjdHIgPSAobmV3IEhNQUModGhpcy5zZWNyZXQpKS5wcm9jZXNzKFwiXCIgKyBsZW5ndGgpLmg7XG4gICAgdmFyIGRlY3J5cHRlZCA9IFtdO1xuXG4gICAgd2hpbGUgKG1lc3NhZ2UubGVuZ3RoID4gMCkge1xuICAgICAgbmV4dF9jaHVuayA9IG1lc3NhZ2Uuc2xpY2UoMCwgOCk7XG5cbiAgICAgIG5leHRfY2h1bmsgPSBCaXQueG9yX2FycmF5KGN0ciwgbmV4dF9jaHVuayk7XG4gICAgICBkZWNyeXB0ZWQgPSBkZWNyeXB0ZWQuY29uY2F0KG5leHRfY2h1bmspO1xuICAgICAgbWVzc2FnZSA9IG1lc3NhZ2Uuc2xpY2UoOCk7XG5cbiAgICAgIGN0ciA9IG5ldyBTSEEyNTYoY3RyLmNvbmNhdChuZXh0X2NodW5rKSk7XG4gICAgICBjdHIuZmluYWxpemUoKTtcbiAgICAgIGN0ciA9IGN0ci5oO1xuICAgIH07XG5cbiAgICBkZWNyeXB0ZWQgPSBCaXQud29yZGFycmF5X3RvX3V0ZjgoZGVjcnlwdGVkLnNsaWNlKDgpKVxuICAgIGRlY3J5cHRlZCA9IGRlY3J5cHRlZC5yZXBsYWNlKC9cXDArJC8sICcnKSAgLy8gUmVtb3ZlIHBhZGRpbmdcblxuICAgIHJldHVybiBkZWNyeXB0ZWQ7XG4gIH07XG5cblxuICBTSEEyNTYgPSBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgdGhpcy5yZXNldCgpO1xuICAgIHRoaXMudXBkYXRlKG1lc3NhZ2UpO1xuICB9O1xuXG5cbiAgU0hBMjU2LnByb3RvdHlwZS5yZXNldCA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBwcmltZXMgPSBmdW5jdGlvbihzdGFydCwgZW5kLCBjYWxsYmFjaykge1xuICAgICAgZm9yICh2YXIgbnVtID0gc3RhcnQ7IG51bSA8IGVuZDsgbnVtKyspIHtcbiAgICAgICAgdmFyIHByaW1lID0gdHJ1ZTtcbiAgICAgICAgZm9yICh2YXIgZGl2ID0gMjsgZGl2KmRpdiA8PSBudW07IGRpdisrKSB7XG4gICAgICAgICAgaWYgKG51bSAlIGRpdiA9PT0gMCkge1xuICAgICAgICAgICAgcHJpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGRpdiA9IG51bTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHByaW1lKSB7XG4gICAgICAgICAgY2FsbGJhY2sobnVtKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICB2YXIgZnJhY3Rpb24gPSBmdW5jdGlvbihudW0pIHtcbiAgICAgIHJldHVybiAoKG51bSAlIDEpICogMHgxMDAwMDAwMDApID4+PiAwO1xuICAgIH07XG5cbiAgICB0aGlzLmggPSBbXTtcbiAgICB0aGlzLmsgPSBbXTtcbiAgICB0aGlzLmJ1ZmZlciA9IFtdO1xuICAgIHRoaXMubGVuZ3RoID0gMDtcbiAgICB0aGlzLmZpbmFsaXplZCA9IGZhbHNlO1xuXG4gICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgIHByaW1lcygyLCAzMTIsIGZ1bmN0aW9uKHByaW1lKSB7XG4gICAgICBpZiAocHJpbWUgPCAyMCkge1xuICAgICAgICB0aGF0LmgucHVzaChmcmFjdGlvbihNYXRoLnBvdyhwcmltZSwgMS8yKSkpO1xuICAgICAgfVxuICAgICAgdGhhdC5rLnB1c2goZnJhY3Rpb24oTWF0aC5wb3cocHJpbWUsIDEvMykpKTtcbiAgICB9KTtcbiAgfTtcblxuXG4gIC8vIFRPRE86IFRlc3RzIGlmIGluIHRoZSByaWdodCBzdGF0ZVxuICBTSEEyNTYucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uKG1lc3NhZ2UsIGVuY29kZSkge1xuICAgIHZhciBuZXh0X2NodW5rO1xuICAgIGlmICh0eXBlb2YgZW5jb2RlID09ICd1bmRlZmluZWQnKSB7XG4gICAgICBlbmNvZGUgPSB0cnVlO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmZpbmFsaXplZCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ2FuJ3QgdXBkYXRlIGhhc2ggYmVjYXVzZSBpdCdzIGFscmVhZHkgaW4gZmluYWwgc3RhdGVcIik7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBtZXNzYWdlID09PSBcInN0cmluZ1wiKSB7XG4gICAgICBpZiAoZW5jb2RlKSB7XG4gICAgICAgIG1lc3NhZ2UgPSBCaXQudXRmOF9lbmNvZGUobWVzc2FnZSk7XG4gICAgICB9XG4gICAgICB0aGlzLmJ1ZmZlciA9IHRoaXMuYnVmZmVyLmNvbmNhdChCaXQudXRmOF90b193b3JkYXJyYXkobWVzc2FnZSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmJ1ZmZlciA9IHRoaXMuYnVmZmVyLmNvbmNhdChtZXNzYWdlKTtcbiAgICB9XG4gICAgdGhpcy5sZW5ndGggKz0gbWVzc2FnZS5sZW5ndGg7XG5cbiAgICB3aGlsZSAodGhpcy5idWZmZXIubGVuZ3RoID4gMTYpIHtcbiAgICAgIG5leHRfY2h1bmsgPSB0aGlzLmJ1ZmZlci5zbGljZSgwLCAxNik7XG5cbiAgICAgIHRoaXMucHJvY2VzcyhuZXh0X2NodW5rKTtcbiAgICAgIHRoaXMuYnVmZmVyID0gdGhpcy5idWZmZXIuc2xpY2UoMTYpO1xuICAgIH1cbiAgfTtcblxuXG4gIFNIQTI1Ni5wcm90b3R5cGUuZmluYWxpemUgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgd29yZCwgbGFzdF9jaHVuaywgbGFzdF9ieXRlLCBmaW5hbF9jaHVua3MsIHBhZGRpbmc7XG4gICAgdGhpcy5maW5hbGl6ZWQgPSB0cnVlO1xuXG4gICAgaWYgKHRoaXMubGVuZ3RoICUgNCA9PSAwKSB7XG4gICAgICB0aGlzLmJ1ZmZlci5wdXNoKDB4ODAgPDwgMjQpXG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBsYXN0X2luZGV4ID0gdGhpcy5idWZmZXIubGVuZ3RoIC0gMTtcbiAgICAgIHRoaXMuYnVmZmVyW2xhc3RfaW5kZXhdIF49ICgweDgwIDw8ICg4ICogKDMgLSAodGhpcy5sZW5ndGggJSA0KSkpKVxuICAgIH1cblxuICAgIHdvcmQgPSB0aGlzLmJ1ZmZlci5sZW5ndGggLSAxO1xuICAgIGxhc3RfY2h1bmsgPSAoKHdvcmQrMiA+PiA0KSsxIDw8IDQpO1xuICAgIGxhc3RfYnl0ZSA9IGxhc3RfY2h1bmsgPDwgMjtcblxuICAgIGZpbmFsX2NodW5rcyA9IHRoaXMuYnVmZmVyO1xuICAgIHZhciBwYWRkaW5nID0gW107XG4gICAgdmFyIHBhZGRpbmdfbGVuZ3RoID0gbGFzdF9jaHVuayAtIHRoaXMuYnVmZmVyLmxlbmd0aDtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhZGRpbmdfbGVuZ3RoOyBpKyspIHtcbiAgICAgIHBhZGRpbmdbaV0gPSAwO1xuICAgIH1cblxuICAgIGZpbmFsX2NodW5rcyA9IGZpbmFsX2NodW5rcy5jb25jYXQocGFkZGluZyk7XG4gICAgZmluYWxfY2h1bmtzW2xhc3RfY2h1bmstMl0gfD0gKHRoaXMubGVuZ3RoIC8gMHgxMDAwMDAwMDApIHwgMDtcbiAgICBmaW5hbF9jaHVua3NbbGFzdF9jaHVuay0xXSB8PSB0aGlzLmxlbmd0aCA8PCAzO1xuXG4gICAgdGhpcy5wcm9jZXNzKGZpbmFsX2NodW5rcyk7XG4gIH07XG5cblxuICBTSEEyNTYucHJvdG90eXBlLnByb2Nlc3MgPSBmdW5jdGlvbih3b3JkYXJyYXkpIHtcbiAgICB2YXIgdywgczAsIHMxLCBoLCBrLCBTMCwgUzEsIGNoLCBtYWosIGQsIHRlbXA7XG5cbiAgICB3ID0gW107XG4gICAgayA9IHRoaXMuay5zbGljZSgpO1xuXG4gICAgZm9yICh2YXIgaT0wOyBpIDwgd29yZGFycmF5Lmxlbmd0aDsgaSArPSAxNikge1xuICAgICAgZm9yICh2YXIgbj0wOyBuIDwgNjQ7IG4rKykge1xuICAgICAgICBpZiAobiA8IDE2KSB7XG4gICAgICAgICAgd1tuXSA9IHdvcmRhcnJheVtpICsgbl07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgczAgPSBCaXQueG9yX2VhY2goQml0LnJvdGF0ZV9lYWNoKHdbbi0xNV0sIFs3LCAxOF0pKSBeICh3W24tMTVdID4+PiAzKTtcbiAgICAgICAgICBzMSA9IEJpdC54b3JfZWFjaChCaXQucm90YXRlX2VhY2god1tuLTJdLCBbMTcsIDE5XSkpIF4gKHdbbi0yXSA+Pj4gMTApO1xuICAgICAgICAgIHdbbl0gPSAod1tuLTE2XSArIHMwICsgd1tuLTddICsgczEpICYgMHhmZmZmZmZmZjtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBoID0gdGhpcy5oLnNsaWNlKCk7XG4gICAgICBmb3IgKHZhciByPTA7IHIgPCA2NDsgcisrKSB7XG4gICAgICAgIFMwID0gQml0Lnhvcl9lYWNoKEJpdC5yb3RhdGVfZWFjaChoWzBdLCBbMiwgMTMsIDIyXSkpO1xuICAgICAgICBTMSA9IEJpdC54b3JfZWFjaChCaXQucm90YXRlX2VhY2goaFs0XSwgWzYsIDExLCAyNV0pKTtcblxuICAgICAgICBtYWogPSAoaFswXSAmIChoWzFdIF4gaFsyXSkpIF4gKGhbMV0gJiBoWzJdKTtcbiAgICAgICAgY2ggPSAoaFs0XSAmIGhbNV0pIF4gKCh+aFs0XSkgJiBoWzZdKTtcbiAgICAgICAgdGVtcCA9IChoWzddICsgUzEgKyBjaCArIGtbcl0gKyB3W3JdKTtcblxuICAgICAgICBoWzNdID0gKGhbM10gKyB0ZW1wKSAmIDB4ZmZmZmZmZmY7XG4gICAgICAgIHRlbXAgPSAodGVtcCArIFMwICsgbWFqKSAmIDB4ZmZmZmZmZmY7XG5cbiAgICAgICAgaC5wb3AoKTtcbiAgICAgICAgaC51bnNoaWZ0KHRlbXApO1xuICAgICAgfVxuXG4gICAgICB2YXIgdGhhdCA9IHRoaXNcbiAgICAgIHRoaXMuaC5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0sIGluZGV4LCBhcnJheSkge1xuICAgICAgICB0aGF0LmhbaW5kZXhdID0gKGl0ZW0gKyBoW2luZGV4XSkgfCAwO1xuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG5cbiAgU0hBMjU2LnByb3RvdHlwZS5oZXhkaWdlc3QgPSBmdW5jdGlvbigpIHtcbiAgICBpZiAoIXRoaXMuZmluYWxpemVkKSB7XG4gICAgICB0aGlzLmZpbmFsaXplKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIEJpdC5oZXhkaWdlc3QodGhpcy5oKTtcbiAgfTtcblxuXG4gIHZhciBITUFDID0gZnVuY3Rpb24oa2V5KSB7XG4gICAgaWYgKHR5cGVvZiBrZXkgIT09IFwic3RyaW5nXCIpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIk9ubHkgc3RyaW5ncyBhcmUgYWxsb3dlZCBhcyBrZXlzXCIpO1xuICAgIH1cbiAgICBpZiAoQml0LnV0ZjhfZW5jb2RlKGtleSkubGVuZ3RoID4gNjQpIHtcbiAgICAgIGhhc2ggPSBuZXcgU0hBMjU2KGtleSk7XG4gICAgICBoYXNoLmZpbmFsaXplKCk7XG4gICAgICB0aGlzLmtleSA9IGhhc2guaDtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIGxlZnQgPSBCaXQudXRmOF90b193b3JkYXJyYXkoa2V5KTtcbiAgICAgIHZhciByaWdodCA9IG5ldyBBcnJheSgxNiAtIGxlZnQubGVuZ3RoKTtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcmlnaHQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgcmlnaHRbaV0gPSAwO1xuICAgICAgfVxuICAgICAgdGhpcy5rZXkgPSBsZWZ0LmNvbmNhdChyaWdodCk7XG4gICAgfVxuICB9O1xuXG5cbiAgSE1BQy5wcm90b3R5cGUucHJvY2VzcyA9IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICB2YXIgbGVuZ3RoLCBvcGFkLCBpcGFkLCBoYXNoLCBsZWZ0LCByaWdodDtcblxuICAgIC8vIFNpemUgb2YgQmxvY2sgdGltZXMgMHg1QyBhbmQgMHgzNlxuICAgIG9wYWQgPSBCaXQudXRmOF90b193b3JkYXJyYXkobmV3IEFycmF5KDY1KS5qb2luKFwiXFxcXFwiKSk7XG4gICAgaXBhZCA9IEJpdC51dGY4X3RvX3dvcmRhcnJheShuZXcgQXJyYXkoNjUpLmpvaW4oXCI2XCIpKTtcblxuICAgIG1lc3NhZ2UgPSBCaXQudXRmOF90b193b3JkYXJyYXkobWVzc2FnZSk7XG5cbiAgICBsZWZ0ID0gQml0Lnhvcl9hcnJheSh0aGlzLmtleSwgb3BhZCk7XG4gICAgcmlnaHQgPSBCaXQueG9yX2FycmF5KHRoaXMua2V5LCBpcGFkKTtcbiAgICByaWdodCA9IHJpZ2h0LmNvbmNhdChtZXNzYWdlKTtcblxuICAgIC8vIGhhc2gob19rZXlfcGFkIOKIpSBoYXNoKGlfa2V5X3BhZCDiiKUgbWVzc2FnZSkpXG4gICAgcmlnaHQgPSBuZXcgU0hBMjU2KHJpZ2h0KTtcbiAgICByaWdodC5maW5hbGl6ZSgpO1xuICAgIHJpZ2h0ID0gcmlnaHQuaDtcblxuICAgIGhhc2ggPSBuZXcgU0hBMjU2KGxlZnQuY29uY2F0KHJpZ2h0KSk7XG4gICAgaGFzaC5maW5hbGl6ZSgpO1xuXG4gICAgcmV0dXJuIGhhc2g7XG4gIH07XG5cblxuICBITUFDLnByb3RvdHlwZS5zaWduID0gZnVuY3Rpb24obWVzc2FnZSkge1xuICAgIHZhciBoYXNoID0gdGhpcy5wcm9jZXNzKG1lc3NhZ2UpLmguc2xpY2UoMCwzKTtcbiAgICByZXR1cm4gYnRvYShtZXNzYWdlKSArIFwifFwiICsgYnRvYShCaXQud29yZGFycmF5X3RvX3V0ZjgoaGFzaCkpO1xuICB9O1xuXG5cbiAgSE1BQy5wcm90b3R5cGUudW5zaWduID0gZnVuY3Rpb24obWVzc2FnZSkge1xuICAgIHZhciBoYXNoLCBobWFjO1xuICAgIG1lc3NhZ2UgPSBtZXNzYWdlLnNwbGl0KFwifFwiKTtcblxuICAgIGhhc2ggPSB0aGlzLnByb2Nlc3MoYXRvYihtZXNzYWdlWzBdKSkuaC5zbGljZSgwLDMpO1xuICAgIGhtYWMgPSBidG9hKEJpdC53b3JkYXJyYXlfdG9fdXRmOChoYXNoKSk7XG4gICAgaWYgKG1lc3NhZ2VbMV0gIT09IGhtYWMpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkJhZCBzaWduYXR1cmUhXCIpO1xuICAgIH1cblxuICAgIHJldHVybiBhdG9iKG1lc3NhZ2VbMF0pO1xuICB9O1xuXG5cbiAgQml0ID0ge1xuICAgIHJpZ2h0cm90YXRlOiBmdW5jdGlvbih3b3JkLCBzaGlmdCkge1xuICAgICAgcmV0dXJuICh3b3JkID4+PiBzaGlmdCkgfCAod29yZCA8PCAoMzIgLSBzaGlmdCkpO1xuICAgIH0sXG5cbiAgICB4b3JfZWFjaDogZnVuY3Rpb24odmFsdWVzKSB7XG4gICAgICByZXR1cm4gdmFsdWVzLnJlZHVjZShmdW5jdGlvbih2YWx1ZSwgYWRkKSB7XG4gICAgICAgIHJldHVybiB2YWx1ZSBeIGFkZDtcbiAgICAgIH0pO1xuICAgIH0sXG5cbiAgICByb3RhdGVfZWFjaDogZnVuY3Rpb24od29yZCwgc2hpZnRzKSB7XG4gICAgICB2YXIgd29yZHMsIHRoYXQgPSB0aGlzO1xuICAgICAgcmV0dXJuIHNoaWZ0cy5tYXAoZnVuY3Rpb24oc2hpZnQpe1xuICAgICAgICByZXR1cm4gdGhhdC5yaWdodHJvdGF0ZSh3b3JkLCBzaGlmdCk7XG4gICAgICB9KTtcbiAgICB9LFxuXG4gICAgeG9yX2FycmF5OiBmdW5jdGlvbihsZWZ0LCByaWdodCkge1xuICAgICAgcmVzdWx0ID0gW107XG4gICAgICBsZWZ0LmZvckVhY2goZnVuY3Rpb24oaXRlbSwgaW5kZXgsIGFycmF5KSB7XG4gICAgICAgIHJlc3VsdFtpbmRleF0gPSBpdGVtIF4gcmlnaHRbaW5kZXhdO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0sXG5cbiAgICB1dGY4X3RvX3dvcmRhcnJheTogZnVuY3Rpb24oc3RyLCBsZW5ndGgpIHtcbiAgICAgIGlmICh0eXBlb2YgbGVuZ3RoID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICBsZW5ndGggPSBzdHIubGVuZ3RoO1xuICAgICAgfVxuXG4gICAgICB2YXIgd29yZGFycmF5ID0gW107XG4gICAgICBmb3IgKHZhciBpPTA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICB3b3JkYXJyYXlbaSA+PiAyXSB8PSBzdHIuY2hhckNvZGVBdChpKSA8PCAoOCAqICgzIC0gaSU0KSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gd29yZGFycmF5O1xuICAgIH0sXG5cbiAgICB3b3JkYXJyYXlfdG9fdXRmODogZnVuY3Rpb24od29yZGFycmF5KSB7XG4gICAgICB2YXIgc3RyID0gXCJcIjtcbiAgICAgIHdvcmRhcnJheS5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0sIGluZGV4LCBhcnJheSkge1xuICAgICAgICBmb3IgKHZhciBpPTA7IGkgPCA0OyBpKyspIHtcbiAgICAgICAgICBzdHIgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgoaXRlbSA+Pj4gKCgzLWkpICogOCkpICYgMHhmZik7XG4gICAgICAgIH07XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBzdHI7XG4gICAgfSxcblxuICAgIHV0ZjhfZW5jb2RlOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICByZXR1cm4gdW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KG1lc3NhZ2UpKTtcbiAgICB9LFxuXG4gICAgdXRmOF9kZWNvZGU6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgIHJldHVybiBkZWNvZGVVUklDb21wb25lbnQoZXNjYXBlKG1lc3NhZ2UpKTtcbiAgICB9LFxuXG4gICAgZGlnZXN0OiBmdW5jdGlvbihtZXNzYWdlLCBudW0sIGxlbmd0aCkge1xuICAgICAgdmFyIHJlc3VsdCA9IFwiXCI7XG4gICAgICB2YXIgcGFkID0gKG5ldyBBcnJheShsZW5ndGggKyAxKSkuam9pbignMCcpO1xuICAgICAgbWVzc2FnZS5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0sIGluZGV4LCBhcnJheSkge1xuICAgICAgICBpdGVtID0gaXRlbSA+Pj4gMDtcbiAgICAgICAgcmVzdWx0ICs9IChwYWQgKyBpdGVtLnRvU3RyaW5nKG51bSkpLnNsaWNlKC1sZW5ndGgpO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0sXG5cbiAgICBoZXhkaWdlc3Q6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgIHJldHVybiB0aGlzLmRpZ2VzdChtZXNzYWdlLCAxNiwgOCk7XG4gICAgfSxcblxuICAgIGJpbmRpZ2VzdDogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgcmV0dXJuIHRoaXMuZGlnZXN0KG1lc3NhZ2UsIDIsIDMyKTtcbiAgICB9XG4gIH07XG5cbiAgd2luZG93LkJpdCA9IEJpdDtcbiAgd2luZG93LkhNQUMgPSBITUFDO1xuICB3aW5kb3cuU0hBMjU2ID0gU0hBMjU2O1xuICB3aW5kb3cuQ3J5cHRvID0gQ3J5cHRvO1xufSk7XG4iLCIod2luZG93Lm1haW4gPSBmdW5jdGlvbiAoKSB7XG4gIHZhciBzb2NrID0gbmV3IFNvY2tldChcbiAgICAnd3M6Ly8xMjcuMC4wLjE6ODAwMC9zZXJ2ZXInLFxuICAgICdEaWUgaXN0IGVpbiBTZWtyZXQnXG4gICk7XG4gIHZhciB0ID0gbmV3IFRlbXBsYXRlcygpO1xuXG4gIHZhciBpbml0X2NhbnZhcyA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBjYW52YXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBsYXlfYXJlYVwiKTtcbiAgICBjYW52YXMud2lkdGggPSBNYXRoLmZsb29yKGNhbnZhcy5wYXJlbnROb2RlLmNsaWVudFdpZHRoKTtcbiAgICBjYW52YXMuaGVpZ2h0ID0gTWF0aC5mbG9vcih3aW5kb3cuaW5uZXJIZWlnaHQpO1xuICAgIHZhciBjdHggPSBjYW52YXMuZ2V0Q29udGV4dChcIjJkXCIpO1xuXG4gICAgdmFyIGxpbmVfZXZlcnkgPSBjYW52YXMuaGVpZ2h0IC8gMzA7XG4gICAgZm9yICh2YXIgaSA9IGxpbmVfZXZlcnk7IGkgPCBjYW52YXMuaGVpZ2h0OyBpICs9IGxpbmVfZXZlcnkpIHtcbiAgICAgIGN0eC5saW5lV2lkdGggPSAwLjU7XG4gICAgICBjdHguc3Ryb2tlU3R5bGUgPSBcInJnYmEoNDEsMzYsMzIsMC42KVwiO1xuICAgICAgY3R4LmJlZ2luUGF0aCgpO1xuICAgICAgdmFyIGNlaWwgPSBNYXRoLmNlaWwoaSkrMC41XG4gICAgICBjdHgubW92ZVRvKDAsIGNlaWwpO1xuICAgICAgY3R4LmxpbmVUbyhjYW52YXMud2lkdGgsIGNlaWwpO1xuICAgICAgY3R4LnN0cm9rZSgpO1xuICAgIH1cbiAgfTtcblxuICB2YXIgaGVhZGVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWRlcicpWzBdO1xuICBoZWFkZXIub25jbGljayA9IGZ1bmN0aW9uKCkge1xuICAgIGxvY2F0aW9uLmhhc2ggPSAnaW5kZXgnO1xuICB9O1xuXG4gIFJvdXRlID0gZnVuY3Rpb24oc3RhcnQpIHtcbiAgICBsb2NhdGlvbi5oYXNoID0gc3RhcnQ7XG4gICAgdGhpc1tsb2NhdGlvbi5oYXNoLnNsaWNlKDEpXSgpO1xuICAgIHRoaXMuY3VycmVudCA9IGxvY2F0aW9uLmhhc2guc2xpY2UoMSk7XG4gICAgdmFyIHRoYXQgPSB0aGlzO1xuXG4gICAgd2luZG93Lm9uaGFzaGNoYW5nZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKHR5cGVvZiB0aGF0LmN1cnJlbnQgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgdGhhdFt0aGF0LmN1cnJlbnQgKyBcIl9leGl0XCJdKCk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICBpZiAoIShlIGluc3RhbmNlb2YgUmVmZXJlbmNlRXJyb3IpKSB7XG4gICAgICAgICAgICB0aHJvdyBlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgdGhhdFtsb2NhdGlvbi5oYXNoLnNsaWNlKDEpXSgpO1xuICAgICAgdGhhdC5jdXJyZW50ID0gbG9jYXRpb24uaGFzaC5zbGljZSgxKTtcbiAgICB9O1xuICB9O1xuXG5cbiAgUm91dGUucHJvdG90eXBlLmluZGV4ID0gZnVuY3Rpb24oKSB7XG4gICAgc29jay5jbGllbnQuam9pbl9ncm91cChudWxsKTtcblxuICAgIC8vIERpc3BsYXkgV2VsY29tZVxuICAgIHZhciB3ZWxjb21lID0gdC5yZW5kZXIoJ3dlbGNvbWUnKTtcbiAgICB2YXIgY29udGVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb250ZW50Jyk7XG4gICAgY29udGVudC5pbm5lckhUTUwgPSB3ZWxjb21lO1xuXG4gICAgLy8gVXNlcm5hbWUgY2hhbmdlclxuICAgIHZhciBzaWRlYmFyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NvbnRyb2xzJyk7XG4gICAgdmFyIG5hbWVfZm9ybSA9IHQucmVuZGVyKCduYW1lX2Zvcm0nKTtcbiAgICBzaWRlYmFyLmluc2VydEFkamFjZW50SFRNTCgnYmVmb3JlZW5kJywgbmFtZV9mb3JtKTtcbiAgICB2YXIgdXNlcm5hbWUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndXNlcm5hbWUnKTtcbiAgICB1c2VybmFtZS52YWx1ZSA9IHNvY2suY2xpZW50Lm5hbWUgfHwgJyc7XG5cbiAgICBuYW1lX2Zvcm0gPSB1c2VybmFtZS5wYXJlbnROb2RlO1xuICAgIG5hbWVfZm9ybS5vbnN1Ym1pdCA9IGZ1bmN0aW9uKGUpIHtcbiAgICAgIGNvbnNvbGUubG9nKHVzZXJuYW1lLnZhbHVlKTtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHNvY2suY2xpZW50LmNoYW5nZV9uYW1lKHVzZXJuYW1lLnZhbHVlKTtcbiAgICB9O1xuXG4gICAgLy8gQXBwZW5kIEdyb3VwbmFtZSBIYW5kbGVyXG4gICAgdmFyIGdyb3VwX25hbWUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZ3JvdXBfbmFtZScpO1xuICAgIHZhciBncm91cF9mb3JtID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dyb3VwX2Zvcm0nKTtcbiAgICBncm91cF9mb3JtLm9uc3VibWl0ID0gZnVuY3Rpb24oZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgc29jay5jbGllbnQuam9pbl9ncm91cChncm91cF9uYW1lLnZhbHVlKTtcbiAgICAgIGxvY2F0aW9uLmhhc2ggPSAnZ3JvdXAnO1xuICAgIH07XG4gICAgZ3JvdXBfbmFtZS5mb2N1cygpO1xuXG4gICAgLy8gQ3JlYXRlIEdyb3VwIFdyYXBwZXJcbiAgICB2YXIgZ3JvdXBzX3RhZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3VsJyk7XG4gICAgZ3JvdXBzX3RhZy5jbGFzc05hbWUgPSBcImdyb3Vwc1wiO1xuICAgIHNpZGViYXIuYXBwZW5kQ2hpbGQoZ3JvdXBzX3RhZyk7XG5cbiAgICAvLyBSZW5kZXIgZnVuY3Rpb24gZ3JvdXAgZGlzcGxheVxuICAgIHZhciByZW5kZXJfZ3JvdXBzID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgZ3JvdXBzID0gc29jay5nZXRfZ3JvdXBzKCk7XG5cbiAgICAgIGdyb3Vwc190YWcuaW5uZXJIVE1MID0gXCJcIjtcbiAgICAgIGZvciAodmFyIGdyb3VwIGluIGdyb3Vwcykge1xuICAgICAgICB2YXIgdXNlcnMgPSBncm91cHNbZ3JvdXBdLmZpbHRlcihmdW5jdGlvbihjbGllbnQpIHtcbiAgICAgICAgICByZXR1cm4gY2xpZW50Lm5hbWUgIT09IG51bGxcbiAgICAgICAgfSk7XG4gICAgICAgIHZhciBhbm9ueW1vdXMgPSBncm91cHNbZ3JvdXBdLmxlbmd0aCAtIHVzZXJzLmxlbmd0aDtcblxuICAgICAgICB1c2VycyA9IHVzZXJzLm1hcChmdW5jdGlvbihjbGllbnQpIHtcbiAgICAgICAgICByZXR1cm4ge3VzZXJfbmFtZTogY2xpZW50Lm5hbWV9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHZhciBlbnRyeSA9IHQucmVuZGVyKCdncm91cF9lbnRyeScsIHtcbiAgICAgICAgICAgIG5hbWU6IGdyb3VwLFxuICAgICAgICAgICAgdXNlcnM6IFsndXNlcl9lbnRyeScsIHVzZXJzXVxuICAgICAgICB9LCB0cnVlKTtcbiAgICAgICAgZW50cnkgPSBlbnRyeS5maXJzdEVsZW1lbnRDaGlsZDtcblxuICAgICAgICBlbnRyeS5kYXRhc2V0Lm5hbWUgPSBncm91cDtcbiAgICAgICAgZW50cnkub25jbGljayA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHNvY2suY2xpZW50LmpvaW5fZ3JvdXAoZW50cnkuZGF0YXNldC5uYW1lKTtcbiAgICAgICAgICBsb2NhdGlvbi5oYXNoID0gJ2dyb3VwJztcbiAgICAgICAgfTtcbiAgICAgICAgZ3JvdXBzX3RhZy5hcHBlbmRDaGlsZChlbnRyeSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIC8vIEludm9rZSByZW5kZXIgZnVuY3Rpb24gb24gam9pbiBhbmQgZ3JvdXAgY2hhbmdlXG4gICAgc29jay5vbl9ncm91cF9jaGFuZ2UgPSByZW5kZXJfZ3JvdXBzO1xuICAgIHNvY2sub25fam9pbiA9IHJlbmRlcl9ncm91cHM7XG4gICAgc29jay5vbl9uYW1lX2NoYW5nZSA9IHJlbmRlcl9ncm91cHM7XG4gICAgc29jay5vbl9sZWF2ZSA9IHJlbmRlcl9ncm91cHM7XG5cbiAgICBpbml0X2NhbnZhcygpO1xuICAgIHdpbmRvdy5vbnJlc2l6ZSA9IGluaXRfY2FudmFzO1xuICB9O1xuXG4gIFJvdXRlLnByb3RvdHlwZS5pbmRleF9leGl0ID0gZnVuY3Rpb24oKSB7XG4gICAgZGVsZXRlIHNvY2sub25fZ3JvdXBfY2hhbmdlO1xuICAgIGRlbGV0ZSBzb2NrLm9uX2pvaW47XG4gICAgZGVsZXRlIHNvY2sub25fbmFtZV9jaGFuZ2U7XG4gICAgZGVsZXRlIHNvY2sub25fbGVhdmU7XG5cbiAgICB2YXIgc2lkZWJhciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb250cm9scycpO1xuICAgIHNpZGViYXIuaW5uZXJIVE1MID0gJyc7XG5cbiAgICB2YXIgY29udGVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb250ZW50Jyk7XG4gICAgY29udGVudC5pbm5lckhUTUwgPSAnJztcbiAgfTtcblxuICBSb3V0ZS5wcm90b3R5cGUuZ3JvdXAgPSBmdW5jdGlvbigpIHtcbiAgICBpZiAoIXNvY2suY2xpZW50Lmdyb3VwKSB7XG4gICAgICBsb2NhdGlvbi5oYXNoID0gJ2luZGV4JztcbiAgICB9XG5cbiAgICB2YXIgc2lkZWJhciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjb250cm9scycpO1xuICAgIHZhciByZW5kZXJfY2hhdCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGNoYXQgPSB0LnJlbmRlcignY2hhdCcpO1xuICAgICAgc2lkZWJhci5pbnNlcnRBZGphY2VudEhUTUwoJ2JlZm9yZWVuZCcsIGNoYXQpO1xuICAgICAgY2hhdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjaGF0Jyk7XG5cbiAgICAgIHNvY2sub25fbWVzc2FnZSA9IGZ1bmN0aW9uKGNsaWVudCwgZGF0YSkge1xuICAgICAgICB2YXIgbWVzc2FnZXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbWVzc2FnZXMnKTtcbiAgICAgICAgY29uc29sZS5sb2coY2xpZW50Lm5hbWUpO1xuICAgICAgICBjb25zb2xlLmxvZyhkYXRhLm1zZyk7XG4gICAgICAgIHZhciBtZXNzYWdlID0gdC5yZW5kZXIoJ21lc3NhZ2UnLCB7XG4gICAgICAgICAgJ3VzZXInOiBjbGllbnQubmFtZSxcbiAgICAgICAgICAnbXNnX2NvbnRlbnQnOiBkYXRhXG4gICAgICAgIH0pO1xuICAgICAgICBtZXNzYWdlcy5pbnNlcnRBZGphY2VudEhUTUwoJ2JlZm9yZWVuZCcsIG1lc3NhZ2UpO1xuICAgICAgICBtZXNzYWdlcy5zY3JvbGxUb3AgPSBtZXNzYWdlcy5zY3JvbGxIZWlnaHQ7XG4gICAgICB9O1xuXG4gICAgICB2YXIgZm9ybSA9IGNoYXQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2Zvcm0nKVswXTtcbiAgICAgIHZhciBpbnB1dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZXNzYWdlX2lucHV0Jyk7XG4gICAgICBmb3JtLm9uc3VibWl0ID0gZnVuY3Rpb24oZSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgaWYgKGlucHV0LnZhbHVlKSB7XG4gICAgICAgICAgc29jay5zZW5kX2V2ZW50KCdtZXNzYWdlJywgaW5wdXQudmFsdWUpO1xuICAgICAgICAgIHNvY2sub25fbWVzc2FnZShzb2NrLmNsaWVudCwgaW5wdXQudmFsdWUpO1xuICAgICAgICB9XG4gICAgICAgIGlucHV0LnZhbHVlID0gXCJcIjtcbiAgICAgIH07XG4gICAgICBpbnB1dC5mb2N1cygpO1xuICAgIH1cblxuICAgIGlmIChzb2NrLmNsaWVudC5uYW1lICE9PSBudWxsKSB7XG4gICAgICByZW5kZXJfY2hhdCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgbmFtZV9mb3JtID0gdC5yZW5kZXIoJ25hbWVfZm9ybScpO1xuICAgICAgc2lkZWJhci5pbnNlcnRBZGphY2VudEhUTUwoJ2JlZm9yZWVuZCcsIG5hbWVfZm9ybSk7XG4gICAgICB2YXIgdXNlcm5hbWUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndXNlcm5hbWUnKTtcbiAgICAgIG5hbWVfZm9ybSA9IHVzZXJuYW1lLnBhcmVudE5vZGU7XG4gICAgICBuYW1lX2Zvcm0ub25zdWJtaXQgPSBmdW5jdGlvbihlKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKHVzZXJuYW1lLnZhbHVlKTtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBzb2NrLmNsaWVudC5jaGFuZ2VfbmFtZSh1c2VybmFtZS52YWx1ZSk7XG4gICAgICAgIG5hbWVfZm9ybS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKG5hbWVfZm9ybSk7XG4gICAgICAgIHJlbmRlcl9jaGF0KCk7XG4gICAgICB9O1xuICAgICAgdXNlcm5hbWUuZm9jdXMoKTtcbiAgICB9XG4gIH07XG5cbiAgUm91dGUucHJvdG90eXBlLmdyb3VwX2V4aXQgPSBmdW5jdGlvbigpIHtcbiAgICBkZWxldGUgc29jay5vbl9tZXNzYWdlO1xuXG4gICAgdmFyIHNpZGViYXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY29udHJvbHMnKTtcbiAgICBzaWRlYmFyLmlubmVySFRNTCA9ICcnO1xuXG4gICAgdmFyIGNvbnRlbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY29udGVudCcpO1xuICAgIGNvbnRlbnQuaW5uZXJIVE1MID0gJyc7XG4gIH07XG5cbiAgdC5sb2FkKCkudGhlbihmdW5jdGlvbigpIHtcbiAgICB2YXIgcm91dGUgPSBuZXcgUm91dGUobG9jYXRpb24uaGFzaC5zbGljZSgxKSB8fCAnaW5kZXgnKTtcbiAgfSwgZnVuY3Rpb24oKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdTaGl0Jyk7XG4gIH0pO1xufSk7XG4iLCIod2luZG93LnNvY2tldCA9IGZ1bmN0aW9uICgpIHtcbiAgU29ja2V0ID0gZnVuY3Rpb24odXJsLCBzZWNyZXQpIHtcbiAgICB0aGlzLnNvY2sgPSBuZXcgV2ViU29ja2V0KHVybCk7XG4gICAgdGhpcy5zZWNyZXQgPSBzZWNyZXQ7XG4gICAgdGhpcy5jcnlwdCA9IG5ldyBDcnlwdG8oc2VjcmV0KTtcbiAgICB0aGlzLmhtYWMgPSBuZXcgSE1BQyhzZWNyZXQgKyBcInNpZ25cIik7XG5cbiAgICB0aGlzLmhlYXJ0YmVhdHMgPSAwO1xuICAgIHRoaXMuY2xpZW50ID0gbmV3IENsaWVudCh0aGlzKTtcbiAgICB0aGlzLmNsaWVudHMgPSBbXTtcblxuICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICB0aGlzLnNvY2sub25tZXNzYWdlID0gZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgdGhhdC5fb25tZXNzYWdlKG1lc3NhZ2UpO1xuICAgIH07XG5cbiAgICB0aGlzLnNvY2sub25vcGVuID0gZnVuY3Rpb24oKSB7XG4gICAgICBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcbiAgICAgICAgdGhhdC5zZW5kX2hlYXJ0YmVhdCgpO1xuICAgICAgICB0aGF0LmNsZWFuX2NsaWVudHMoKTtcbiAgICAgIH0sIDMwMDApO1xuICAgIH07XG5cbiAgICB0aGlzLnNvY2sub25jbG9zZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhhdC5jbGVhbl9jbGllbnRzKHRydWUpO1xuICAgIH07XG4gIH07XG5cbiAgU29ja2V0LnByb3RvdHlwZS5yYXdfc2VuZCA9IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICBtZXNzYWdlID0gdGhpcy5jcnlwdC5lbmNyeXB0KG1lc3NhZ2UpO1xuICAgIG1lc3NhZ2UgPSB0aGlzLmhtYWMuc2lnbihtZXNzYWdlKTtcblxuICAgIGlmICh0aGlzLnNvY2sucmVhZHlTdGF0ZSAhPT0gMSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdTb2NrZXQgaXNuXFwndCBjb25uZWN0ZWQnKTtcbiAgICB9XG4gICAgaWYgKG1lc3NhZ2UubGVuZ3RoID4gMjAwMCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdNZXNzYWdlIHRvIGJpZycpO1xuICAgIH1cblxuICAgIHRoaXMuc29jay5zZW5kKG1lc3NhZ2UpO1xuICB9O1xuXG4gIFNvY2tldC5wcm90b3R5cGUuY2xlYW5fY2xpZW50cyA9IGZ1bmN0aW9uKGFsbCkgIHtcbiAgICB2YXIgbm93ID0gKG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcbiAgICBpZiAodHlwZW9mIGFsbCA9PSAndW5kZWZpbmVkJykge1xuICAgICAgYWxsID0gZmFsc2U7XG4gICAgfVxuXG4gICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgIHRoaXMuY2xpZW50cy5mb3JFYWNoKGZ1bmN0aW9uKGNsaWVudCwgaW5kZXgsIGFycmF5KSB7XG4gICAgICBpZiAoYWxsIHx8IChub3cgLSBjbGllbnQubGFzdF9oZWFydGJlYXQpID4gMTAwMDApIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0NsaWVudCBsZWF2ZWQ6ICcgKyBjbGllbnQuZ2lkKTtcbiAgICAgICAgZGVsZXRlIGFycmF5W2luZGV4XTtcbiAgICAgICAgaWYgKHR5cGVvZiB0aGF0Lm9uX2xlYXZlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgdGhhdC5vbl9sZWF2ZShjbGllbnQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gICAgdGhpcy5jbGllbnRzID0gdGhpcy5jbGllbnRzLmZpbHRlcihmdW5jdGlvbihpdGVtKSB7XG4gICAgICByZXR1cm4gaXRlbSAhPT0gJ3VuZGVmaW5lZCc7XG4gICAgfSk7XG4gIH07XG5cbiAgU29ja2V0LnByb3RvdHlwZS5nZXRfZ3JvdXBzID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGdyb3VwcyA9IHt9O1xuICAgIHRoaXMuY2xpZW50cy5mb3JFYWNoKGZ1bmN0aW9uKGNsaWVudCwgaW5kZXgsIGFycmF5KSB7XG4gICAgICBpZiAoY2xpZW50Lmdyb3VwKSB7XG4gICAgICAgIGlmICghKGNsaWVudC5ncm91cCBpbiBncm91cHMpKSB7XG4gICAgICAgICAgZ3JvdXBzW2NsaWVudC5ncm91cF0gPSBbXTtcbiAgICAgICAgfVxuICAgICAgICBncm91cHNbY2xpZW50Lmdyb3VwXS5wdXNoKGNsaWVudCk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICByZXR1cm4gZ3JvdXBzO1xuICB9O1xuXG4gIFNvY2tldC5wcm90b3R5cGUuc2VuZF9oZWFydGJlYXQgPSBmdW5jdGlvbigpIHtcbiAgICBtZXNzYWdlID0ge1xuICAgICAgdHlwZTogJ2JlYXQnLFxuICAgICAgZ2lkOiB0aGlzLmNsaWVudC5naWRcbiAgICB9O1xuXG4gICAgaWYgKHRoaXMuY2xpZW50Lmdyb3VwICE9PSBudWxsKSB7XG4gICAgICBtZXNzYWdlWydncm91cCddID0gdGhpcy5jbGllbnQuZ3JvdXA7XG4gICAgfVxuICAgIGlmICh0aGlzLmNsaWVudC5uYW1lICE9PSBudWxsKSB7XG4gICAgICBtZXNzYWdlWyduYW1lJ10gPSB0aGlzLmNsaWVudC5uYW1lO1xuICAgIH1cbiAgICB0aGlzLnJhd19zZW5kKEpTT04uc3RyaW5naWZ5KG1lc3NhZ2UpKTtcbiAgICB0aGlzLmNsaWVudC5oZWFydGJlYXRzICs9IDE7XG4gIH07XG5cbiAgU29ja2V0LnByb3RvdHlwZS5zZW5kX2V2ZW50ID0gZnVuY3Rpb24obmFtZSwgbXNnKSB7XG4gICAgbWVzc2FnZSA9IHtcbiAgICAgIHR5cGU6ICdldmVudCcsXG4gICAgICBnaWQ6IHRoaXMuY2xpZW50LmdpZCxcbiAgICAgIG5hbWU6IG5hbWUsXG4gICAgICBtc2c6IEpTT04uc3RyaW5naWZ5KG1zZylcbiAgICB9O1xuICAgIGNvbnNvbGUubG9nKG1lc3NhZ2UpO1xuICAgIHRoaXMucmF3X3NlbmQoSlNPTi5zdHJpbmdpZnkobWVzc2FnZSkpO1xuICB9XG5cbiAgU29ja2V0LnByb3RvdHlwZS5wcm9jZXNzX2JlYXQgPSBmdW5jdGlvbihkYXRhKSB7XG4gICAgdmFyIGNsaWVudCA9IHRoaXMuY2xpZW50cy5maWx0ZXIoZnVuY3Rpb24oY2xpZW50KSB7XG4gICAgICByZXR1cm4gY2xpZW50LmdpZCA9PT0gZGF0YS5naWQ7XG4gICAgfSlbMF07XG5cbiAgICBpZiAodHlwZW9mIGNsaWVudCA9PSAndW5kZWZpbmVkJykge1xuICAgICAgY29uc29sZS5sb2coJ05ldyBjbGllbnQ6ICcgKyBkYXRhLmdpZCk7XG4gICAgICBjbGllbnQgPSBuZXcgQ2xpZW50KHRoaXMsIGRhdGEuZ2lkLCBkYXRhLmdyb3VwIHx8IG51bGwpO1xuICAgICAgY2xpZW50Lm5hbWUgPSBkYXRhLm5hbWUgfHwgbnVsbDtcbiAgICAgIHRoaXMuY2xpZW50cy5wdXNoKGNsaWVudCk7XG5cbiAgICAgIGlmICh0aGlzLmNsaWVudC5oZWFydGJlYXRzID4gMSkge1xuICAgICAgICBjb25zb2xlLmxvZygnSW50cm9kdWNlIScpO1xuICAgICAgICB0aGlzLnNlbmRfaGVhcnRiZWF0KCk7XG4gICAgICB9XG5cbiAgICAgIGNsaWVudC5sYXN0X2hlYXJ0YmVhdCA9IChuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XG4gICAgICBpZiAodHlwZW9mIHRoaXMub25fam9pbiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLm9uX2pvaW4oY2xpZW50KTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgY2xpZW50Lmxhc3RfaGVhcnRiZWF0ID0gKG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcblxuICAgICAgdmFyIGdyb3VwID0gZGF0YS5ncm91cCB8fCBudWxsO1xuICAgICAgaWYgKGNsaWVudC5ncm91cCAhPT0gZ3JvdXApIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0NsaWVudCcgKyBjbGllbnQuZ2lkICsgJyBqb2luZWQgZ3JvdXA6ICcgKyBncm91cCk7XG4gICAgICAgIGNsaWVudC5ncm91cCA9IGdyb3VwO1xuICAgICAgICBpZiAodHlwZW9mIHRoaXMub25fZ3JvdXBfY2hhbmdlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgdGhpcy5vbl9ncm91cF9jaGFuZ2UoY2xpZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgdmFyIG5hbWUgPSBkYXRhLm5hbWUgfHwgbnVsbDtcbiAgICAgIGlmIChjbGllbnQubmFtZSAhPT0gbmFtZSkge1xuICAgICAgICBjb25zb2xlLmxvZygnQ2xpZW50JyArIGNsaWVudC5naWQgKyAnIGNoYW5nZWQgbmFtZTogJyArIG5hbWUpO1xuICAgICAgICBjbGllbnQubmFtZSA9IG5hbWU7XG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5vbl9uYW1lX2NoYW5nZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIHRoaXMub25fbmFtZV9jaGFuZ2UoY2xpZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBTb2NrZXQucHJvdG90eXBlLl9vbm1lc3NhZ2UgPSBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgdmFyIGRhdGE7XG4gICAgdHJ5IHtcbiAgICAgIGRhdGEgPSB0aGlzLmhtYWMudW5zaWduKG1lc3NhZ2UuZGF0YSk7XG4gICAgICBkYXRhID0gdGhpcy5jcnlwdC5kZWNyeXB0KGRhdGEpO1xuICAgICAgLy8gUGFyc2UgYXMgSlNPTiAod2Ugb25seSBzZW5kIGpzb24pXG4gICAgICBkYXRhID0gSlNPTi5wYXJzZShkYXRhKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAvLyBITUFDIGZhaWxlZC4gV2UgZGlkbid0IHNlbmQgdGhlIG1lc3NhZ2UuXG4gICAgICBkYXRhID0gbWVzc2FnZS5kYXRhO1xuICAgICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgfVxuXG4gICAgaWYgKGRhdGEudHlwZSA9PT0gJ2JlYXQnKSB7XG4gICAgICB0aGlzLnByb2Nlc3NfYmVhdChkYXRhKTtcbiAgICB9IGVsc2UgaWYgKGRhdGEudHlwZSA9PT0gJ2V2ZW50Jykge1xuICAgICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgICB2YXIgY2xpZW50ID0gdGhpcy5jbGllbnRzLmZpbHRlcihmdW5jdGlvbihjbGllbnQpIHtcbiAgICAgICAgcmV0dXJuIGNsaWVudC5naWQgPT09IGRhdGEuZ2lkO1xuICAgICAgfSlbMF07XG4gICAgICBjb25zb2xlLmxvZyhjbGllbnQpO1xuICAgICAgaWYgKHR5cGVvZiB0aGlzWydvbl8nICsgZGF0YS5uYW1lXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBjb25zb2xlLmxvZygnU293ZWl0IHNvZ3V0OiBsb2wnKTtcbiAgICAgICAgY29uc29sZS5sb2coY2xpZW50KTtcbiAgICAgICAgdGhpc1snb25fJyArIGRhdGEubmFtZV0oY2xpZW50LCBKU09OLnBhcnNlKGRhdGEubXNnKSk7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIHdpbmRvdy5Tb2NrZXQgPSBTb2NrZXQ7XG59KTtcbiIsIih3aW5kb3cudGVtcGxhdGVzID0gZnVuY3Rpb24gKCkge1xuICBUZW1wbGF0ZXMgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgdGhpcy5sb2FkZWQgPSBmYWxzZTtcbiAgICB0aGlzLnRlbXBsYXRlcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RlbXBsYXRlcycpO1xuICB9O1xuXG4gIFRlbXBsYXRlcy5wcm90b3R5cGUubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMuc3RhcnRlZCA9IChuZXcgRGF0ZSkuZ2V0VGltZSgpO1xuXG4gICAgdmFyIGh0dHBSZXF1ZXN0O1xuICAgIGlmICh3aW5kb3cuWE1MSHR0cFJlcXVlc3QpIHtcbiAgICAgIGh0dHBSZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgfSBlbHNlIGlmICh3aW5kb3cuQWN0aXZlWE9iamVjdCkge1xuICAgICAgaHR0cFJlcXVlc3QgPSBuZXcgQWN0aXZlWE9iamVjdChcIk1pY3Jvc29mdC5YTUxIVFRQXCIpO1xuICAgIH1cblxuICAgIHZhciB0aGF0ID0gdGhpcztcblxuICAgIHZhciBzY3JpcHQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndGVtcGxhdGVzJyk7XG4gICAgaHR0cFJlcXVlc3Qub3BlbignR0VUJywgc2NyaXB0LnNyYywgdHJ1ZSk7XG4gICAgaHR0cFJlcXVlc3Quc2VuZChudWxsKTtcblxuICAgIHZhciBwcm9taXNlID0gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICBodHRwUmVxdWVzdC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpe1xuICAgICAgICBpZiAoaHR0cFJlcXVlc3QucmVhZHlTdGF0ZSA9PT0gNCkge1xuICAgICAgICAgIGlmIChodHRwUmVxdWVzdC5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgdGhhdC50ZW1wbGF0ZXMuaW5uZXJIVE1MID0gaHR0cFJlcXVlc3QucmVzcG9uc2VUZXh0O1xuICAgICAgICAgICAgdGhhdC5sb2FkZWQgPSB0cnVlO1xuICAgICAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfSk7XG5cbiAgICByZXR1cm4gcHJvbWlzZTtcbiAgfTtcblxuICBUZW1wbGF0ZXMucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uKG5hbWUsIGRhdGEsIHdyYXApIHtcbiAgICB2YXIgdGVtcGxhdGUgPSB0aGlzLnRlbXBsYXRlcy5xdWVyeVNlbGVjdG9yKCcjJyArIG5hbWUpO1xuXG4gICAgdmFyIHRlbXAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICB0ZW1wLmlubmVySFRNTCA9IHRlbXBsYXRlLmlubmVySFRNTDtcbiAgICB0ZW1wLmlkID0gbmFtZTtcblxuICAgIGRhdGEgPSBkYXRhIHx8IHt9O1xuICAgIHRoYXQgPSB0aGlzO1xuXG4gICAgZm9yICh2YXIgaXRlbSBpbiBkYXRhKSB7XG4gICAgICB2YXIgdGFncyA9IFtdLnNsaWNlLmNhbGwodGVtcC5xdWVyeVNlbGVjdG9yQWxsKCcuJyArIGl0ZW0pKTtcbiAgICAgIHRhZ3MuZm9yRWFjaChmdW5jdGlvbih0YWcpIHtcbiAgICAgICAgaWYgKGRhdGFbaXRlbV0gaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICAgIGRhdGFbaXRlbV1bMV0uZm9yRWFjaChmdW5jdGlvbihpbm5lckl0ZW0pIHtcbiAgICAgICAgICAgIHZhciBpbm5lciA9IHRoYXQucmVuZGVyKGRhdGFbaXRlbV1bMF0sIGlubmVySXRlbSk7XG4gICAgICAgICAgICB0YWcuaW5zZXJ0QWRqYWNlbnRIVE1MKCdiZWZvcmVlbmQnLCBpbm5lcik7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgaWYgKGRhdGFbaXRlbV1bMV0ubGVuZ3RoIDwgMSkge1xuICAgICAgICAgICAgdGFnLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGFnKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGFnLmlubmVySFRNTCA9IGRhdGFbaXRlbV07XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmICh0eXBlb2Ygd3JhcCAhPT0gJ3VuZGVmaW5lZCcgJiYgd3JhcCkge1xuICAgICAgcmV0dXJuIHRlbXA7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0ZW1wLmlubmVySFRNTDtcbiAgICB9XG4gIH07XG5cbiAgd2luZG93LlRlbXBsYXRlcyA9IFRlbXBsYXRlcztcbn0pO1xuIiwiKGZ1bmN0aW9uICgpIHtcbiAgd2luZG93LmNsaWVudCgpO1xuICB3aW5kb3cuY3J5cHQoKTtcbiAgd2luZG93LnNvY2tldCgpO1xuICB3aW5kb3cudGVtcGxhdGVzKCk7XG4gIHdpbmRvdy5tYWluKCk7XG59KCkpO1xuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9